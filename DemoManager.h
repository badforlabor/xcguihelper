#pragma once

#include "CPath.h"
#include "SysSetFile.h"
#include <shlwapi.h>
#pragma comment(lib,"Shlwapi.lib")


VOID InitAppCurrentDir();
const wchar_t* GetAppDir();


class CMemSetFile
{
public:
	BOOL Init(const wchar_t *pFileName)
	{
		return doc.load_file(pFileName);
	}
	const wchar_t* GetDataStr(const wchar_t* pNode,const wchar_t* pAttName)
	{
		return doc.child(pNode).attribute(pAttName).as_string();
	}
	float GetDataFloat(const wchar_t* pNode,const wchar_t* pAttName)
	{
		return doc.child(pNode).attribute(pAttName).as_float();
	}
	int GetDataInt(const wchar_t* pNode,const wchar_t* pAttName)
	{
		return doc.child(pNode).attribute(pAttName).as_int();
	}
private:
	pugi::xml_document doc;
};

class CPage1
{
public:
	CPage1();
	~CPage1();

	template_info_i m_ListBoxTemplateItemSelect;
	template_info_i m_ListBoxTemplateItemStay;

	HELE m_hLastItemEle;
	int  nMouseInItemID;
	int OnListboxMouseMove(UINT nFlags, POINT *pPt, BOOL *pbHandled);

	int OnListBoxDrawItem(HDRAW hDraw,listBox_item_i* pItem,BOOL *pbHandled);

	int OnListBoxTemplateCreate(listBox_item_i* pItem,BOOL *pbHandled);
	int OnListBoxSelect(int iItem,BOOL *pbHandled);
	VOID Create(HWINDOW hParentWindow);


	int OnListboxMouseStay(BOOL *pbHandled);
	int OnListboxMouseLeave(HELE hEleStay,BOOL *pbHandled);

	int OnComboBoxSelect(int iItem,BOOL *pbHandled);
	//下拉组合框点击，弹出下来列表
	int OnLButtonUp(UINT nFlags, POINT *pPt,BOOL *pbHandled);
	//工程版本选中
	int OnButtonCheck(HELE hEle,HELE hEventEle,BOOL bCheck,BOOL *pbHandled);
	//工程版本双击
	int OnLButtonDBClick(HELE hEle,UINT nFlags, POINT *pPt,BOOL *pbHandled);


	int OnListBoxTemplateCreateEnd(listBox_item_i* pItem,BOOL *pbHandled);
	//生成代码
	int OnBtnCreateCodeClick(HELE hEventEle,BOOL* pbHandled);
	//查看代码
	int OnBtnViewCodeClick(HELE hEventEle,BOOL* pbHandled);
	//查看预览
	int OnBtnViewPicClick( HELE hEventEle,BOOL* pbHandled );

	//搜索回调函数
	HELE m_hEditSearch;
	int OnEventKeyDown(WPARAM wParam,LPARAM lParam,BOOL *pbHandled);
	//分类事件
	int OnShowBtnClick(HELE hEventEle,BOOL *pbHandled);

	int OnWndDestroy(HWINDOW hWindow,BOOL *pbHandled);


	int OnPicViewWndMove(HWINDOW hWindow,WPARAM wParam,LPARAM lParam,BOOL *pbHandled);


private:

	//
	void GetSetFileFullPath(int nIndex,wchar_t* pBuffer);

	//例如 demo.gif
	BOOL IsFileExist(int nSelect ,const wchar_t* pFileName);

	void ShowCategory(const wchar_t* lpCategory);
	HELE CreateGrounpButton(wchar_t* pbuttonName,HXCGUI hParent,int nGrounpID);

private:
	wchar_t m_pCurDir[MAX_PATH_XC];
	wchar_t m_pTempDir[MAX_PATH_XC];
	wchar_t m_pDemoMgrDir[MAX_PATH_XC];
	wchar_t m_pCppDir[MAX_PATH_XC];


	wchar_t m_language[MAX_PATH_XC]; //编程语言

	HELE    m_hBtnSlnVer;


	HELE m_hListBox;
	HXCGUI m_hAdapterListBox;

	HELE m_hComboBox;
	HXCGUI hAdapterComBoBox;

	HXCGUI m_hLayoutSlnVer;

	HELE m_hEditPath;

	HWINDOW m_hParent;

	HIMAGE  m_hImageHard[11];

	HWINDOW m_hWindowPicView;
	RECT    m_RectPicWindow;
	
};

//炫彩教程
class CPage2
{
public:
	CPage2();
	VOID Create(HWINDOW hParentWindow);

//event
public:
	int OnLButtonUp(UINT nFlags, POINT *pPt,BOOL *pbHandled);

	int OnComboBoxSelect(int iItem,BOOL *pbHandled);

	int OnListBoxTemplateCreateEnd(listBox_item_i* pItem,BOOL *pbHandled);

	int OnBtnClick(HELE hEventEle,BOOL *pbHandled);

	int OnEventKeyDown(WPARAM wParam,LPARAM lParam,BOOL *pbHandled);


	int OnCategoryBtnClick(HELE hEventEle, BOOL *pbHandled);


private:

	void ShowCategory(const wchar_t* lpCategory);

private:
	wchar_t m_TechPath[MAX_PATH_XC];
	HWINDOW m_hParentWindow;

	HELE    m_hListBox;
	HXCGUI  m_hAdapterTableListBox;

	HELE	m_hComboBox;
	HXCGUI  m_hAdapterTableComboBox;

	HELE    m_hRichEdit;
};

//ui脚本
class CPage3
{
public:
	CPage3();
	
	VOID Create(HWINDOW hParentWindow);
	
	
public:
	int OnComboBoxLButtonUp( UINT nFlags, POINT *pPt,BOOL *pbHandled );
	int OnComboBoxSelect( int iItem,BOOL *pbHandled );

	int OnEventKeyDown(WPARAM wParam,LPARAM lParam,BOOL *pbHandled);

	int OnListBoxTemplateCreateEnd(listBox_item_i* pItem,BOOL *pbHandled);
	int OnBtnClick(HELE hEventEle,BOOL *pbHandled);
	int OnBtnCategoryClick(HELE hEventEle,BOOL *pbHandled);
private:

	void ShowCategory(const wchar_t* lpCategory);
private:
	HWINDOW m_hParentWindow;
	wchar_t m_pLuaUIMgrDir[MAX_PATH_XC];
	

	HELE    m_hComboBox;
	HXCGUI  m_hAdapterComboBox;

	HELE    m_hListBox;
	HXCGUI  m_hAdapterTableListBox;
	HELE    m_hRichEdit;

};


class CWindowXML
{
public:
	CWindowXML();
	int OnBtnClick(BOOL *pbHandled);
	VOID Create();
	int OnHeadBtnClick(HELE hEventEle, BOOL *pbHandled);

	int OnWndSetTextLinkText(WPARAM wParam,LPARAM lParam,BOOL *pbHandled);
	int OnTextLinkClick(BOOL* pbHandled);


	int OnWndSize(UINT nFlags,SIZE *pSize,BOOL *pbHandled);
	int OnWndMove(WPARAM wParam,LPARAM lParam,BOOL *pbHandled);

private:
	HWINDOW m_hWindow;

	HELE   hBtn1;
	HELE   hBtn2;
	HELE   hBtn3;
	HELE   hBtn4;

	HELE   hPage1;
	HELE   hPage2;
	HELE   hPage3;

	CPage2  m_Page2;
	CPage1  m_Page1;
	CPage3  m_Page3;

	HELE    m_hEditPath;
	HELE hTextLink;
};


class CCmdLine
{
public:
	CCmdLine();
	wchar_t* operator[](int nIndex);
	~CCmdLine();
public:
	int     m_nArc;
private:
	LPWSTR* m_lpCmd;
};

class CConsole
{
public:
	CConsole(const wchar_t* pText = L"Debug Output")
	{
		AllocConsole();						// 开辟控制台
		SetConsoleTitleW(pText);			// 设置控制台窗口标题
		freopen("CONOUT$","w",stdout);		// 重定向输出
	}
	~CConsole()
	{
		FreeConsole();
	}
private:
};
