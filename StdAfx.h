// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_)
#define AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_

#pragma warning(disable:4786)

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

#include <windows.h>


#if (_MSC_VER == 1100) //5.0

#endif

#if (_MSC_VER == 1200)
//6.0
	#define strcpy_s strcpy
#endif

#if (_MSC_VER == 1600) // 2010

#endif

#include <ShellAPI.h>

#include "xcgui.h"
#pragma comment(lib,"xcgui.lib")


#define ZeroMaxPathXC(pBuffer)  ZeroMemory(pBuffer,sizeof(wchar_t)*MAX_PATH_XC)

void MyGetDirectory(wchar_t* pDir, int size);
unsigned long XC_GetFileSize(wchar_t* pFile,unsigned long * pFileSizeHight);
VOID XC_OpenFloder(wchar_t* pDir);
int XC_CopyFile(wchar_t* pSrc,wchar_t* pDes);
BOOL XC_IsDirectory(wchar_t* lpFile);
BOOL XC_ReNameFolder(const wchar_t* lpszFromPath,const wchar_t* lpszToPath);
BOOL XC_DeleteFolder(const wchar_t* lpszPath);
bool XC_IsFileExsit(const wchar_t* file);

HXCGUI XC_GetObjectByRes(HWINDOW hWindow,const wchar_t* lpResIDName);

#define XC_GetHEleByID(HWINDOW_hWindow, int_nID) ((HELE)XC_GetObjectByID(HWINDOW_hWindow,int_nID))
HELE XC_GetHEleByRes(HWINDOW hWindow,const wchar_t* lpResIDName);
HXCGUI XC_GetShapeByRes(HWINDOW hWindow,const wchar_t* lpResIDName);


#define XC_XML_ROOT L"DemoManager"



#define XC_PATH_ROOT  L"data"
#define XC_DIRNAME_TEMP  L"Temp"
#define XC_DIRNAME_DEMO  L"DemoMgr"
#define XC_DIRNAME_CODE  L"CodeMgr"
#define XC_DIRNAME_EVENT L"EventMgr"
#define XC_DIRNAME_TECH L"TechMgr"
#define XC_DIRNAME_CODESLN L"Sln"
#define XC_DIRNAME_CODELIB L"Lib"
#define XC_DIRNAME_CODECPP L"cpp"


#define XC_PATH_TEMP L"data\\Temp"
#define XC_PATH_DEMO L"data\\DemoMgr"
#define XC_PATH_CODE L"data\\CodeMgr"
#define XC_PATH_LUAUI L"data\\luaUIMgr"
#define XC_PATH_EVENT L"data\\EventMgr"
#define XC_PATH_LAYOUTRES_DIR L"data\\res"
#define XC_PATH_LAYOUTRES_IMAGE_DIR L"data\\res\\Image"
#define XC_PATH_XCGUILIB  L"data\\xcgui"



#define XC_FULLPATH_IMAGE_MAINWND_PATH    L"data\\res\\Image\\ClassicHover"



#define XC_FULLPATH_SETFILE L"data\\Set.data"
#define XC_FULLPATH_LAYOUT_RES_FILE    L"data\\res\\resource.xml"
#define XC_FULLPATH_LAYOUT_FILE L"data\\res\\layout.xml"
#define XC_FULLPATH_LAYOUT_PAGE1_COMBOBOX L"data\\res\\ComboBox_ListBox_Item.xml"
#define XC_FULLPATH_LAYOUT_PAGE1_LISTBOX  L"data\\res\\Page1_ListBox_Item.xml"
#define XC_FULLPATH_LAYOUT_PAGE1_LISTBOX_STAY  L"data\\res\\Page1_ListBox_Item_Stay.xml"
#define XC_FULLPATH_LAYOUT_PAGE1_LISTBOX_SELECT  L"data\\res\\Page1_ListBox_Item_Select.xml"
#define XC_FULLPATH_LAYOUT_PAGE1_DEMOGIF  L"data\\res\\DemoGif.xml"
#define XC_FULLPATH_LAYOUT_PAGE1_DEMOPIC  L"data\\res\\DemoPic.xml"

#define XC_FULLPATH_IMAGE_PAGE1_LIST_SCROLLBARV_LEAVE L"data\\res\\Image\\All_vertscrollbar_arrowDownNormal.png"
#define XC_FULLPATH_IMAGE_PAGE1_LIST_SCROLLBARV_HOVER L"data\\res\\Image\\All_vertscrollbar_arrowDownHover.png"
#define XC_FULLPATH_IMAGE_PAGE1_LIST_SCROLLBARV_DOWN L"data\\res\\Image\\All_vertscrollbar_arrowDownDown.png"



#define XC_FULLPATH_LAYOUT_PAGE2_LISTBOX  L"data\\res\\Page2_ListBox_Item.xml"
#define XC_FULLPATH_LAYOUT_PAGE2_COMBOBOX L"data\\res\\ComboBox_ListBox_Item.xml"


#define XC_FULLPATH_LAYOUT_PAGE3_LISTBOX  L"data\\res\\Page3_ListBox_Item.xml"
#define XC_FULLPATH_LAYOUT_PAGE3_TREE L"data\res\\Tree_Item.xml"
#define XC_FULLPATH_LAYOUT_PAGE3_COMBOBOX L"data\\res\\ComboBox_ListBox_Item.xml"



#define       ID_DemoGif       1000 
#define       ID_DemoPic       1100 
#define       ID_HelperVer       25 
#define       ID_Main_button1       800 
#define       ID_Main_button2       801 
#define       ID_Main_button3       802 
#define       ID_Main_button4       803 
#define       ID_editPath       501 
#define       ID_page1       200 
#define       ID_page1_btnSearch       206 
#define       ID_page1_btn_list       220 
#define       ID_page1_btn_listbox       218 
#define       ID_page1_btn_listview       216 
#define       ID_page1_btn_menu       219 
#define       ID_page1_btn_showBtn       215 
#define       ID_page1_btn_showWindow       214 
#define       ID_page1_btn_showall       210 
#define       ID_page1_btn_showcomplete       211 
#define       ID_page1_btn_showele       212 
#define       ID_page1_btn_showlayout       213 
#define       ID_page1_btn_showuserDraw       217 
#define       ID_page1_btn_tree       221 
#define       ID_page1_comboBox       203 
#define       ID_page1_editSearch       205 
#define       ID_page1_layoutSlnVer       204 
#define       ID_page1_listbox       202 
#define       ID_page2       300 
#define       ID_page2_btnserch       303 
#define       ID_page2_combobox_lang       304 
#define       ID_page2_editserch       302 
#define       ID_page2_layout1       306 
#define       ID_page2_layout2       307 
#define       ID_page2_layout3       308 
#define       ID_page2_listbox       301 
#define       ID_page3       400 
#define       ID_page3_combobox       404 
#define       ID_page3_editserch       403 
#define       ID_page3_layou2       411 
#define       ID_page3_layout1       410 
#define       ID_page3_layout3       412 
#define       ID_page3_listbox       401 
#define       ID_page4       500 




#define MAX_PATH_XC    4096

#define XCGUIHELPERVER 12
#define XcguiVer   L"Ver 1.9.9"



// TODO: reference additional headers your program requires here

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_)
