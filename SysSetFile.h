#ifndef SysSetFile_h__
#define SysSetFile_h__

#include <iostream>

#include "pugixml.hpp"



class CEnCoding
{
public:
	CEnCoding();
	~CEnCoding();
	VOID A2W(const char* pText);
	VOID W2A(const wchar_t* pText);
	int GetLen();
	wchar_t* GetStrW();
	char* GetStrA();
	
	VOID Free();
private:
	wchar_t* m_pBufW;
	char*    m_pBufA;
};


class CSaveData
{
public:
	CSaveData();
	~CSaveData();
	VOID Save();

	BOOL IsExsitSetFile()
	{
		return m_IsExistSetFile;
	}
	
	VOID Create();

	int GetMainTabSelect()
	{
		return m_doc.child(XC_XML_ROOT).child(L"MainWindow").attribute(L"sel").as_int();
	}
	VOID SetMainTabSelect(int nSelect)
	{
		m_doc.child(XC_XML_ROOT).child(L"MainWindow").attribute(L"sel").set_value(nSelect);
	}

	VOID SetPage2LangIndex(int nIndex)
	{
		m_doc.child(XC_XML_ROOT).child(L"page2").child(L"langIndex").attribute(L"index").set_value(nIndex);
	}
	int GetPage2LangIndex()
	{
		return m_doc.child(XC_XML_ROOT).child(L"page2").child(L"langIndex").attribute(L"index").as_int();
	}

	VOID SetPage3LangIndex(int nIndex)
	{
		m_doc.child(XC_XML_ROOT).child(L"page3").child(L"langIndex").attribute(L"index").set_value(nIndex);
	}
	int GetPage3LangIndex()
	{
		return m_doc.child(XC_XML_ROOT).child(L"page3").child(L"langIndex").attribute(L"index").as_int();
	}



	const wchar_t* GetCodePath();
	VOID SetCodePath(wchar_t* pPath);
	
	int GetSlnVer();
	BOOL SetSlnVer(int nVer);
	int GetSln();
	VOID SetSln(int nIndex);

	VOID SetLang(int nIndex);
	int GetLang();

	VOID SetLangIndex(int nIndex);
	int GetLangIndex();

	VOID SetWindowPos(int x,int y);
	VOID GetWindowPos(int& x,int &y);
	VOID SetWindowSize(int cx,int cy);
	VOID GetWindowSize(int &cx,int &cy);




private:
	pugi::xml_document m_doc;
	wchar_t pDataPath[MAX_PATH_XC];

	BOOL m_IsExistSetFile;
};


class CXFile
{
public:
	CXFile();
	~CXFile();
	DWORD GetSize(const wchar_t* pFile,DWORD* pdwSizeHight = NULL);
	BOOL Load(const wchar_t* lpFile);
	BOOL Save(const wchar_t* lpFile,const wchar_t* pData);
	wchar_t* GetStr();
private:
	VOID Free();
	
private:
	int m_nSize;
	wchar_t* m_pBuffer;
};




#endif // SysSetFile_h__