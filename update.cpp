#include "StdAfx.h"
#include "update.h"


bool HasNewVer()
{
	CNetFile netFile;
	if (netFile.Read(L"http://www.xcgui.com/bbs/forum.php?mod=viewthread&tid=1638&extra=page%3D1") != NULL)
	{
		std::string ver;
		char* flags = "XCGUIHelper{";
		char* pStart = StrStrA(netFile.GetStr(),"XCGUIHelper{");
		if (pStart == NULL)
		{
			return false;
		}
		char* pEnd   = StrStrA(pStart,"}");
		if (pEnd == NULL)
		{
			return false;
		}
		ver.append(pStart+strlen(flags),pEnd);

		if (atoi(ver.c_str()) > XCGUIHELPERVER)
		{
			return true;
		}
		
	}
	
	return false;
}
