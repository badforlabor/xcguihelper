#include "StdAfx.h"
#include "CPath.h"


////////////////////////////////////////////////////////
CEnumPath::~CEnumPath()
{
	for (int i = 0; i < m_nCount; i++)
	{
		if (m_Path[i] != NULL)
		{
			delete [] m_Path[i];
			m_Path[i] = NULL;
		}
	}
	m_nCount = 0;
}
CEnumPath::CEnumPath(const wchar_t* pPath,BOOL bDir,BOOL bFullPath):m_nCount(0)
{
	ZeroMemory(m_Path,sizeof(wchar_t*)*MAX_PATH);
	WIN32_FIND_DATAW FileData={0}; 
	wchar_t TempPath[MAX_PATH] = {0};
	wcscpy(TempPath,pPath);
	if (TempPath[wcslen(pPath)] != L'\\')
	{
		wcscat(TempPath,L"\\");
	}
	wcscat(TempPath,L"*.*");
	HANDLE hSearch = FindFirstFileW(TempPath, &FileData); 
	
	if (hSearch == INVALID_HANDLE_VALUE) 
	{
		return;
	}
	BOOL fFinished = FALSE; 
	m_nCount = 0;
	while (!fFinished)
	{
		if ((wcscmp(FileData.cFileName,L".") != 0) && (wcscmp(FileData.cFileName,L"..") != 0))
		{
			if ( (FILE_ATTRIBUTE_DIRECTORY & FileData.dwFileAttributes) != 0 )
			{
				if (bDir == TRUE)
				{
					m_Path[m_nCount] = new wchar_t[MAX_PATH_XC];
					ZeroMaxPathXC(m_Path[m_nCount]);
					if (bFullPath == TRUE)
					{
						wcscat(m_Path[m_nCount],pPath);
						wcscat(m_Path[m_nCount],L"\\");
					}
					wcscat(m_Path[m_nCount],FileData.cFileName);
					m_nCount++;
				}
			}else
			{
				if (bDir == FALSE)
				{
					m_Path[m_nCount] = new wchar_t[MAX_PATH_XC];
					ZeroMaxPathXC(m_Path[m_nCount]);
					if (bFullPath == TRUE)
					{
						wcscat(m_Path[m_nCount],pPath);
						wcscat(m_Path[m_nCount],L"\\");
					}
					wcscat(m_Path[m_nCount],FileData.cFileName);
					m_nCount++;
				}
			}
		}
		ZeroMemory(&FileData,sizeof(FileData));
		if (!FindNextFileW(hSearch, &FileData)) 
		{
			if (GetLastError() == ERROR_NO_MORE_FILES) 
			{ 
				fFinished = TRUE; 
			} 
			else 
			{ 
				FindClose(hSearch);
				return ;
			} 
		}
	}
	FindClose(hSearch);
}

wchar_t* CEnumPath::operator[](int n)
{
	return m_Path[n];
}

int CEnumPath::GetItemCount()
{
	return m_nCount;
}