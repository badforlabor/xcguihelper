#include "StdAfx.h"
#include "SysSetFile.h"


//////////////////////////////////////////////////////
CEnCoding::CEnCoding()
{
	m_pBufW = NULL;
	m_pBufA = NULL;
}
CEnCoding::~CEnCoding()
{
	Free();
}
VOID CEnCoding::A2W(const char* pText)
{
	Free();
	int nLen = strlen(pText)+1 ;
	m_pBufW = new wchar_t[nLen];
	ZeroMemory(m_pBufW,sizeof(wchar_t)*nLen);
	XC_AnsiToUnicode(pText,strlen(pText),m_pBufW,nLen);
}
VOID CEnCoding::W2A(const wchar_t* pText)
{
	Free();
	int nLen = XC_UnicodeToAnsi(pText,-1,NULL,NULL)+1;
	m_pBufA = new char[nLen];
	ZeroMemory(m_pBufA,nLen);
	XC_UnicodeToAnsi(pText,wcslen(pText),m_pBufA,nLen);
}
int CEnCoding::GetLen()
{
	return sizeof(m_pBufA);
}
wchar_t* CEnCoding::GetStrW()
{
	return m_pBufW;
}
char* CEnCoding::GetStrA()
{
	return m_pBufA;
}

VOID CEnCoding::Free()
{
	if (m_pBufW)
	{
		delete m_pBufW;
		m_pBufW = NULL;
	}
	if (m_pBufA)
	{
		delete m_pBufA;
		m_pBufA = NULL;
	}
}
/////////////////////////////////////////////////////////////

CSaveData::CSaveData()
{
	ZeroMaxPathXC(pDataPath);

	wchar_t currentDir[MAX_PATH] = {0};
	MyGetDirectory(currentDir,MAX_PATH);
	wsprintfW(pDataPath,L"%s\\%s",currentDir,XC_FULLPATH_SETFILE);

//	OutputDebugStringW(pDataPath);
	//检测配置文件是否存在，不存在重新创建
	if (!XC_IsFileExsit(pDataPath))
	{
		m_IsExistSetFile = FALSE;
		Create();
	}else
	{
		m_IsExistSetFile = TRUE;
	}
	//加载配置文件，内容是上一次界面选中的一些配置
	if (!m_doc.load_file(pDataPath))
	{
		MessageBoxW(NULL,L"加载系统配置失败!",L"提示",MB_OK);
	}
}

CSaveData::~CSaveData()
{
	Save();
}

VOID CSaveData::Save()
{	
	bool bOk = m_doc.save_file(pDataPath);
	m_doc.print(std::cout);
}

VOID CSaveData::Create()
{
	pugi::xml_node root = m_doc.append_child(XC_XML_ROOT);


	pugi::xml_node NodeMainWindow = root.append_child(L"MainWindow");
	NodeMainWindow.append_attribute(L"x").set_value(10);
	NodeMainWindow.append_attribute(L"y").set_value(10);
	NodeMainWindow.append_attribute(L"cx").set_value(760);
	NodeMainWindow.append_attribute(L"cy").set_value(550);
	NodeMainWindow.append_attribute(L"sel").set_value(0);


	pugi::xml_node NodePage1 = root.append_child(L"page1");

	NodePage1.append_child(XC_DIRNAME_CODESLN).append_attribute(L"Type");
	NodePage1.append_child(L"SlnVer").append_attribute(L"Ver");
	NodePage1.append_child(XC_DIRNAME_TEMP).append_attribute(L"Path");
	NodePage1.append_child(L"lang").append_attribute(L"Type");
	NodePage1.append_child(L"langIndex").append_attribute(L"index");


	pugi::xml_node NodePage2 = root.append_child(L"page2");
	NodePage2.append_child(L"langIndex").append_attribute(L"index");

	pugi::xml_node NodePage3 = root.append_child(L"page3");
	NodePage3.append_child(L"langIndex").append_attribute(L"index");


	//		m_doc.print(std::cout);
	m_doc.save_file(pDataPath);
}
const wchar_t* CSaveData::GetCodePath()
{
	return m_doc.child(XC_XML_ROOT).child(XC_DIRNAME_TEMP).attribute(L"Path").value();
}
VOID CSaveData::SetCodePath(wchar_t* pPath)
{
	m_doc.child(XC_XML_ROOT).child(XC_DIRNAME_TEMP).attribute(L"Path").set_value(pPath);
}

int CSaveData::GetSlnVer()
{
	pugi::xml_node nodeSlnVer = m_doc.child(XC_XML_ROOT).child(L"page1").child(L"SlnVer");
	return nodeSlnVer.attribute(L"Ver").as_int();		
}
BOOL CSaveData::SetSlnVer(int nVer)
{
	pugi::xml_node nodeSlnVer = m_doc.child(XC_XML_ROOT).child(L"page1").child(L"SlnVer");
	return nodeSlnVer.attribute(L"Ver").set_value(nVer);
}
int CSaveData::GetSln()
{

	int nSln = m_doc.child(XC_XML_ROOT).child(L"page1").child(XC_DIRNAME_CODESLN).attribute(L"Type").as_int();
	return  nSln;
}
VOID CSaveData::SetSln(int nIndex)
{
	pugi::xml_attribute attrib = m_doc.child(XC_XML_ROOT).child(L"page1").child(XC_DIRNAME_CODESLN).attribute(L"Type");
	attrib.set_value(nIndex);
}

VOID CSaveData::SetLang(int nIndex)
{
	pugi::xml_attribute attrib = m_doc.child(XC_XML_ROOT).child(L"page1").child(L"lang").attribute(L"Type");
	attrib.set_value(nIndex);
}

int CSaveData::GetLang()
{
	
	int nSln = m_doc.child(XC_XML_ROOT).child(L"page1").child(L"lang").attribute(L"Type").as_int();
	return  nSln;
}

VOID CSaveData::SetLangIndex(int nIndex)
{
	pugi::xml_attribute attrib = m_doc.child(XC_XML_ROOT).child(L"page1").child(L"langIndex").attribute(L"index");
	attrib.set_value(nIndex);
}
int CSaveData::GetLangIndex()
{
	int nSln = m_doc.child(XC_XML_ROOT).child(L"page1").child(L"langIndex").attribute(L"Index").as_int();
	return  nSln;
}

VOID CSaveData::SetWindowPos( int x,int y)
{
	m_doc.child(XC_XML_ROOT).child(L"MainWindow").attribute(L"x").set_value(x);
	m_doc.child(XC_XML_ROOT).child(L"MainWindow").attribute(L"y").set_value(y);

}

VOID CSaveData::GetWindowPos( int& x,int &y)
{
	x = m_doc.child(XC_XML_ROOT).child(L"MainWindow").attribute(L"x").as_int();
	y = m_doc.child(XC_XML_ROOT).child(L"MainWindow").attribute(L"y").as_int();

}

VOID CSaveData::SetWindowSize( int cx,int cy )
{
	m_doc.child(XC_XML_ROOT).child(L"MainWindow").attribute(L"cx").set_value(cx);
	m_doc.child(XC_XML_ROOT).child(L"MainWindow").attribute(L"cy").set_value(cy);
}

VOID CSaveData::GetWindowSize( int &cx,int &cy )
{
	cx = m_doc.child(XC_XML_ROOT).child(L"MainWindow").attribute(L"cx").as_int(760);
	cy = m_doc.child(XC_XML_ROOT).child(L"MainWindow").attribute(L"cy").as_int(550);	
}

/////////////////////////////////////////////////////////////////////////////

CXFile::CXFile()
{
	m_nSize = 0;
	m_pBuffer = NULL;
}
CXFile::~CXFile()
{
	Free();
}
DWORD CXFile::GetSize(const wchar_t* pFile,DWORD* pdwSizeHight)
{
	HANDLE hFile = CreateFileW(pFile,
		GENERIC_READ,FILE_SHARE_READ,
		NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		int nSize = GetFileSize(hFile,pdwSizeHight);
		CloseHandle(hFile);
		return nSize;
	}

	return 0;
}
BOOL CXFile::Load(const wchar_t* lpFile)
{
	//先释放已经加载过的内容
	Free();
	//读取文件内容
	int nSize = GetSize(lpFile);
	FILE* pFile1 = _wfopen(lpFile,L"r");
	char* pNew = new char[nSize+1];
	ZeroMemory(pNew,nSize+1);
	fread(pNew,1,nSize,pFile1);
	fclose(pFile1);

	CEnCoding ED;
	ED.A2W(pNew);
	m_pBuffer = new wchar_t[ nSize+1 ];
	ZeroMemory(m_pBuffer,sizeof(wchar_t)*(nSize+1) );
	CopyMemory(m_pBuffer,ED.GetStrW(),sizeof(wchar_t)*nSize );

	delete [] pNew;
	pNew = NULL;

	return TRUE;
}
BOOL CXFile::Save(const wchar_t* lpFile,const wchar_t* pData)
{
	CEnCoding ed;
	ed.W2A(pData);

	//删除已经存在的文件
	DeleteFileW(lpFile);
	FILE* hFile = _wfopen(lpFile,L"w");
	if (hFile == NULL)
	{
		return FALSE;
	}

	CEnCoding edData;
	edData.W2A(pData);
	fwrite(edData.GetStrA(),1,strlen(edData.GetStrA()),hFile);
	fclose(hFile);

	return TRUE;
}
wchar_t* CXFile::GetStr()
{
	return m_pBuffer;
}
VOID CXFile::Free()
{
	if (m_pBuffer)
	{
		delete []m_pBuffer;
		m_pBuffer = NULL;
	}
}