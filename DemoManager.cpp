// DemoManager.cpp : Defines the entry point for the application.
//

#include "stdafx.h"



#include "update.h"

#include <shellapi.h>
#include <algorithm>
#include "DemoManager.h"




CSaveData m_SaveData;

wchar_t g_pAppDir[MAX_PATH_XC] = {0};

VOID InitAppCurrentDir()
{
	MyGetDirectory(g_pAppDir,MAX_PATH_XC);
	
	if(0!=GetModuleFileNameW(NULL,g_pAppDir,MAX_PATH_XC))
	{
		wchar_t *pFilePath=wcsrchr(g_pAppDir, L'\\' );
		if(pFilePath)
		{
			pFilePath[0]=L'\0';
		}
	}
}

const wchar_t* GetAppDir()
{
	return g_pAppDir;
}


//////////////////////////////////////////////

CPage1::CPage1()
{
	nMouseInItemID = -1;
	ZeroMaxPathXC(m_pCurDir);
	ZeroMaxPathXC(m_pTempDir);
	ZeroMaxPathXC(m_pDemoMgrDir);
	ZeroMaxPathXC(m_pCppDir);

	MyGetDirectory(m_pCurDir,MAX_PATH_XC);

	wsprintfW(m_pTempDir,L"%s\\%s",m_pCurDir,XC_PATH_TEMP);
	wsprintfW(m_pDemoMgrDir,L"%s\\%s",m_pCurDir,XC_PATH_DEMO);

	CreateDirectoryW(m_pDemoMgrDir,NULL);
	CreateDirectoryW(m_pTempDir,NULL);


	m_hWindowPicView = NULL;
//	CEnumPath EnumPath(m_pDemoMgrDir,TRUE,TRUE);
}
CPage1::~CPage1()
{
	m_SaveData.Save();
}

int CPage1::OnComboBoxSelect(int iItem,BOOL *pbHandled)
{
	//因为有的时候可能添加了例子，需要重新加载。。
// 	if (XComboBox_GetSelItem(m_hComboBox) == iItem)
// 	{
// 		return 0;
// 	}

	ZeroMaxPathXC(m_language);
	XAdapterTable_GetItemText(hAdapterComBoBox,iItem,0,m_language,MAX_PATH_XC);


	//第一步，销毁 工程版本
	int nC = XLayout_GetChildCount(m_hLayoutSlnVer);
	for (int i = 0; i < nC; i++)
	{
		HELE hEle = (HELE)XLayout_GetChild(m_hLayoutSlnVer,0);
		XEle_Destroy(hEle);
	}

	//然后 动态创建版本
	wchar_t pDir[MAX_PATH_XC] = {0};
	wsprintfW(pDir,L"%s\\%s\\%s",m_pDemoMgrDir,m_language,XC_DIRNAME_CODESLN);

//	OutputDebugStringW(m_language);


	wcscpy(m_pCppDir,pDir);

	CEnumPath Enum(pDir);

	int nDefaltSelect = m_SaveData.GetSlnVer();
	if (nDefaltSelect > Enum.GetItemCount()-1)
	{
		nDefaltSelect = 0;
	}

	HELE hParent = (HELE)XC_GetObjectByID(m_hParent,200);
	for (int j = 0; j < Enum.GetItemCount(); j++)
	{

		HELE hBtn = CreateGrounpButton(Enum[j],hParent,2);
		XEle_SetUserData(hBtn,j);  //绑定索引		
		XEle_SetFont(hBtn,XFont_Create2(L"微软雅黑",12));


		XBtn_AddBkFill(hBtn,button_state_leave,RGB(255,255,255),255);
		XBtn_AddBkFill(hBtn,button_state_down,RGB(220,220,220),150);
		XBtn_AddBkFill(hBtn,button_state_stay,RGB(230,230,230),255);
		XBtn_AddBkFill(hBtn,button_state_check,RGB(230,230,230),255);

		XEle_RegEventCPP2(hBtn,XE_BUTTON_CHECK,&CPage1::OnButtonCheck);
		XEle_RegEventCPP1(hBtn,XE_LBUTTONDBCLICK,&CPage1::OnLButtonDBClick);

		if (nDefaltSelect == j)
		{
			XBtn_SetCheck(hBtn,TRUE);
			
			XEle_PostEvent(hBtn,hBtn,XE_BUTTON_CHECK,1,NULL);
		}

	}


	//这里需要加载源码

	wchar_t cppPath[MAX_PATH_XC] = {0};
	wsprintfW(cppPath,L"%s\\%s\\%s",m_pDemoMgrDir,m_language,XC_DIRNAME_CODECPP);


	int nCount = XAdapterTable_GetCount(m_hAdapterListBox);
	XAdapterTable_DeleteItemEx(m_hAdapterListBox,0,nCount);


	CEnumPath EnumCpp(cppPath);
	
	for (int k = 0;  k < EnumCpp.GetItemCount();  k++)
	{
		XAdapterTable_AddItemText(m_hAdapterListBox,EnumCpp[k]);
	}
	XEle_RedrawEle(m_hListBox);


	XLayout_AdjustLayout(m_hLayoutSlnVer);
	XEle_RedrawEle(hParent);

	m_SaveData.SetSln(iItem);
	m_SaveData.Save();



	return 0;
}

int CPage1::OnButtonCheck(HELE hEle,HELE hEventEle,BOOL bCheck,BOOL *pbHandled)
{

	//拷贝
// 	wchar_t txt[4096] = {0};
// 	wsprintfW(txt,L"%s\\%s",m_pCppDir,m_strSlnVer);
	m_hBtnSlnVer = hEle;
	int nDefaultSelect = XEle_GetUserData(hEle);
	m_SaveData.SetSlnVer(nDefaultSelect);
	m_SaveData.Save();


	return 0;
}



int CPage1::OnLButtonDBClick( HELE hEle,UINT nFlags, POINT *pPt,BOOL *pbHandled )
{
	//然后 动态创建版本
	wchar_t slnVer[MAX_PATH_XC]={0};
	XBtn_GetText(hEle,slnVer,MAX_PATH_XC);
	wchar_t pDir[MAX_PATH_XC] = {0};
	wsprintfW(pDir,L"%s\\%s\\%s\\%s",m_pDemoMgrDir,m_language,XC_DIRNAME_CODESLN,slnVer);
	
	XC_OpenFloder(pDir);
	*pbHandled = TRUE;
	return 0;
}



int CPage1::OnLButtonUp(UINT nFlags, POINT *pPt,BOOL *pbHandled)
{
	RECT rt;
	XComboBox_GetButtonRect(m_hComboBox,&rt);

	if (pPt->x >= rt.left && pPt->x  <= rt.right && pPt->y >= rt.top && pPt->y <= rt.bottom)
	{
	}else
	{
		rt.left += 1;
		rt.top  += 1;
		XEle_SendEvent(m_hComboBox,m_hComboBox,XE_LBUTTONDOWN,nFlags, (LPARAM)&rt);
	}

	return 0;
}

int CPage1::OnListboxMouseMove( UINT nFlags, POINT *pPt, BOOL *pbHandled )
{
	int nIndex = XListBox_HitTestOffset(m_hListBox,pPt);
	if (nIndex == -1)
	{
		return 0;
	}
	if (XC_IsHELE(m_hLastItemEle))
	{
		XEle_ShowEle(m_hLastItemEle,FALSE);
	}
	HXCGUI hEle = XListBox_GetTemplateObject(m_hListBox,nIndex,12);
	if (XC_IsHELE(hEle))
	{
		nMouseInItemID = nIndex;
		XEle_ShowEle((HELE)hEle,TRUE);
		XEle_RedrawEle(m_hListBox,TRUE);
		m_hLastItemEle = (HELE)hEle;
	}
	return 0;
}

int CPage1::OnListBoxDrawItem( HDRAW hDraw,listBox_item_i* pItem,BOOL *pbHandled )
{
	HXCGUI hEle = XListBox_GetTemplateObject(m_hListBox,pItem->index,12);
	switch (pItem->nState)
	{
	case list_item_state_stay:
		{
			if (XC_IsHELE(hEle))
			{
				XEle_ShowEle((HELE)hEle,TRUE);
			}
		}
		break;
	case list_item_state_leave:
		{
			if (XC_IsHELE(hEle))
			{
				XEle_ShowEle((HELE)hEle,FALSE);
			}
		}
		break;
	}

	if ( (pItem->index%2) == 0)
	{
		if (pItem->nState == list_item_state_leave)
			XDraw_FillRectColor(hDraw,&pItem->rcItem,RGB(255,255,255),255);
		else if (pItem->nState == list_item_state_stay)
			XDraw_FillRectColor(hDraw,&pItem->rcItem,RGB(220,220,220),220);
		else
			XDraw_FillRectColor(hDraw,&pItem->rcItem,RGB(255,255,255),255);

	}else
	{
		if (pItem->nState == list_item_state_leave)
			XDraw_FillRectColor(hDraw,&pItem->rcItem,RGB(213,213,213),50);		
		else if (pItem->nState == list_item_state_stay)
			XDraw_FillRectColor(hDraw,&pItem->rcItem,RGB(220,220,220),220);	
		else
			XDraw_FillRectColor(hDraw,&pItem->rcItem,RGB(213,213,213),50);
	}
	HBKINFOM hBkInfoM = XEle_GetBkInfoManager(m_hListBox);

//	*pbHandled = TRUE;

	xtrace("%d %d\r\n",pItem->nState,GetTickCount()); 
	return 0;
}


int CPage1::OnListBoxTemplateCreate( listBox_item_i* pItem,BOOL *pbHandled )
{
	if (XListBox_GetSelectItem(m_hListBox) == pItem->index)
	{
		pItem->pTempInfo = m_ListBoxTemplateItemSelect;
	}
// 	else
// 	{
// 		POINT pt;
// 		GetCursorPos(&pt);
// 		ScreenToClient(XEle_GetHWND(m_hListBox),&pt);
// 		
// 		XEle_PointWndClientToEleClient(m_hListBox,&pt);
// 		int nIndex = XListBox_HitTestOffset(m_hListBox,&pt);
// 		if (nIndex != -1)
// 		{
// 			pItem->pTempInfo = m_ListBoxTemplateItemStay;
// 		}
// 	}

	
// 	switch (pItem->nState)
// 	{
// 	case list_item_state_stay:
// 		break;
// 	case list_item_state_select:
// 		pItem->pTempInfo = m_ListBoxTemplateItemSelect;
// 		*pbHandled = TRUE;
// 		break;
// 	}

	return 0;
}

int CPage1::OnListBoxSelect( int iItem,BOOL *pbHandled )
{
	XListBox_RefreshData(m_hListBox);
	XEle_RedrawEle(m_hListBox,FALSE);
	return 0;
}


VOID CPage1::Create(HWINDOW hParentWindow)
{
	m_hParent = hParentWindow;
	//Page1
	// 1 ListBox

	wchar_t pathListbox[MAX_PATH_XC] = {0};
	wsprintfW(pathListbox,L"%s\\%s",m_pCurDir,XC_FULLPATH_LAYOUT_PAGE1_LISTBOX);
	m_hListBox = (HELE)XC_GetObjectByID(m_hParent,ID_page1_listbox);
	XListBox_SetItemTemplateXML(m_hListBox,pathListbox);


	wchar_t pathListboxSelect[MAX_PATH_XC] = {0};
	wsprintfW(pathListboxSelect,L"%s\\%s",m_pCurDir,XC_FULLPATH_LAYOUT_PAGE1_LISTBOX_SELECT);
	m_ListBoxTemplateItemSelect = XC_LoadTemplate(XC_LISTBOX,pathListboxSelect);


	wchar_t pathListboxStay[MAX_PATH_XC] = {0};
	wsprintfW(pathListboxStay,L"%s\\%s",m_pCurDir,XC_FULLPATH_LAYOUT_PAGE1_LISTBOX_STAY);
	m_ListBoxTemplateItemStay = XC_LoadTemplate(XC_LISTBOX,pathListboxStay);

	m_hAdapterListBox = XAdapterTable_Create();
	XListBox_BindAdapter(m_hListBox,m_hAdapterListBox);
	XAdapterTable_AddColumn(m_hAdapterListBox,L"name");
	XListBox_EnableMultiSel(m_hListBox,FALSE);

	XListBox_SetItemHeightDefault(m_hListBox,30,50);

 	XListBox_AddItemBkFill(m_hListBox,list_item_state_stay,RGB(220,220,220),220);
 	XListBox_AddItemBkFill(m_hListBox,list_item_state_select,RGB(230,230,230),255);

	XListBox_SetRowSpace(m_hListBox,0);


	XEle_RegEventCPP(m_hListBox,XE_MOUSEMOVE,&CPage1::OnListboxMouseMove);
// 	XEle_RegEventCPP(m_hListBox,XE_MOUSESTAY,&CPage1::OnListboxMouseStay);
// 	XEle_RegEventCPP(m_hListBox,XE_MOUSELEAVE,&CPage1::OnListboxMouseLeave);

	XEle_RegEventCPP(m_hListBox,XE_LISTBOX_DRAWITEM,&CPage1::OnListBoxDrawItem);

	HELE hScrollBarV = XSView_GetScrollBarV(m_hListBox);
	HBKINFOM hBkInfoM = XEle_GetBkInfoManager(hScrollBarV);
	XBkInfoM_SetBkInfo(hBkInfoM,L"{5:1(1)2(26)3(1,0,5,0)10(0)20(1)8(0.00)21(1)22(-657931)23(255);}");


	HELE hBtnUp = XSBar_GetButtonUp(hScrollBarV);
	XBtn_SetStyle(hBtnUp,button_style_default);
	hBkInfoM = XEle_GetBkInfoManager(hBtnUp);
	XBkInfoM_SetBkInfo(hBkInfoM,L"{4:1(64)2(48)3(10,15,0,0)10(0)20(1)8(-90.00)21(1)22(-8355712)23(255);5:1(16)2(15)3(0,0,0,0)10(1)20(0)8(0.00)7(1)11(1)12(-789517)13(255);5:1(16)2(15)3(2,2,2,2)10(0)20(1)8(0.00)21(1)22(-789517)23(255);5:1(32)2(15)3(0,0,0,0)10(1)20(0)8(0.00)7(1)11(1)12(-4144960)13(255);5:1(32)2(15)3(2,2,2,2)10(0)20(1)8(0.00)21(1)22(-4144960)23(255);5:1(64)2(15)3(0,0,0,0)10(1)20(0)8(0.00)7(1)11(1)12(-8355712)13(255);5:1(64)2(15)3(2,2,2,2)10(0)20(1)8(0.00)21(1)22(-8355712)23(255);}");



	HELE hBtnDown = XSBar_GetButtonDown(hScrollBarV);
	XBtn_SetStyle(hBtnDown,button_style_default);
	hBkInfoM = XEle_GetBkInfoManager(hBtnDown);
	XBkInfoM_SetBkInfo(hBkInfoM,L"{4:1(64)2(48)3(10,15,0,0)10(0)20(1)8(-90.00)21(1)22(-8355712)23(255);5:1(16)2(15)3(0,0,0,0)10(1)20(0)8(0.00)7(1)11(1)12(-789517)13(255);5:1(16)2(15)3(2,2,2,2)10(0)20(1)8(0.00)21(1)22(-789517)23(255);5:1(32)2(15)3(0,0,0,0)10(1)20(0)8(0.00)7(1)11(1)12(-4144960)13(255);5:1(32)2(15)3(2,2,2,2)10(0)20(1)8(0.00)21(1)22(-4144960)23(255);5:1(64)2(15)3(0,0,0,0)10(1)20(0)8(0.00)7(1)11(1)12(-8355712)13(255);5:1(64)2(15)3(2,2,2,2)10(0)20(1)8(0.00)21(1)22(-8355712)23(255);}");
	

	HELE hBtnSlider = XSBar_GetButtonSlider(hScrollBarV);
	XBtn_SetStyle(hBtnSlider,button_style_default);
	hBkInfoM = XEle_GetBkInfoManager(hBtnSlider);
	XBkInfoM_SetBkInfo(hBkInfoM,L"{5:1(16)2(15)3(0,0,0,0)10(0)20(1)8(0.00)21(1)22(-789517)23(255);5:1(32)2(15)3(0,0,0,0)10(0)20(1)8(0.00)21(1)22(-4144960)23(255);5:1(64)2(15)3(0,0,0,0)10(0)20(1)8(0.00)21(1)22(-8355712)23(255);}");


	XEle_RegEventCPP(m_hListBox,XE_LISTBOX_TEMP_CREATE,&CPage1::OnListBoxTemplateCreate);
	XEle_RegEventCPP(m_hListBox,XE_LISTBOX_TEMP_CREATE_END,&CPage1::OnListBoxTemplateCreateEnd);
	XEle_RegEventCPP(m_hListBox,XE_LISTBOX_SELECT,&CPage1::OnListBoxSelect);

//
//	XSView_ShowSBarV(m_hListBox,FALSE);

//	XSView_SetScrollBarSize(m_hListBox,10);
	//listbox scorbal设置 滚动条设置
// 	HELE hScorllBarV = XSView_GetScrollBarV(m_hListBox);
// 	HELE hBtnDown = XSBar_GetButtonDown(hScorllBarV);
// 	XBtn_SetStyle(hBtnDown,button_style_default);
// 	XEle_EnableBkTransparent(hBtnDown,TRUE);
// 	HBKINFOM hbkInfoM = XEle_GetBkInfoManager(hBtnDown);
// 	XBkInfoM_Clear(hbkInfoM);
// 
// 	wchar_t lpszImageBtn[MAX_PATH_XC] = {0};
// 	wsprintfW(lpszImageBtn,L"%s\\%s",m_pCurDir,XC_FULLPATH_IMAGE_PAGE1_LIST_SCROLLBARV_LEAVE);
// 	HIMAGE hImage = XImage_LoadFileRect(lpszImageBtn,1,1,11,11);
// 	XBkInfoM_AddImage(hbkInfoM,button_state_flag_leave,hImage);
// 
// 	wsprintfW(lpszImageBtn,L"%s\\%s",m_pCurDir,XC_FULLPATH_IMAGE_PAGE1_LIST_SCROLLBARV_HOVER);
// 	XBkInfoM_AddImage(hbkInfoM,button_state_flag_stay,XImage_LoadFileRect(lpszImageBtn,1,1,11,11));
// 
// 	wsprintfW(lpszImageBtn,L"%s\\%s",m_pCurDir,XC_FULLPATH_IMAGE_PAGE1_LIST_SCROLLBARV_DOWN);
// 	XBkInfoM_AddImage(hbkInfoM,button_state_flag_down,XImage_LoadFileRect(lpszImageBtn,1,1,11,11));


	// 1 ComboBox
	
	m_hComboBox = (HELE)XC_GetObjectByID(m_hParent,ID_page1_comboBox);

// 	wchar_t pathCombox[MAX_PATH] = {0};
// 	wsprintfW(pathCombox,L"%s\\%s",m_pCurDir,XC_FULLPATH_LAYOUT_PAGE1_COMBOBOX);
// 	XComboBox_SetItemTemplateXML(m_hComboBox,pathCombox);


	hAdapterComBoBox = XAdapterTable_Create();
	XComboBox_BindApapter(m_hComboBox,hAdapterComBoBox);
	XAdapterTable_AddColumn(hAdapterComBoBox,L"name");

	CEnumPath EnumPath(m_pDemoMgrDir);
	for (int i = 0; i < EnumPath.GetItemCount(); i++)
	{
		XAdapterTable_AddItemText(hAdapterComBoBox,EnumPath[i]); 
	}
	//确保所有的都显示
	XComboBox_SetDropHeight(m_hComboBox,EnumPath.GetItemCount()*25);


	// 还需要设置当前选中项,
	XEle_RegEventCPP(m_hComboBox,XE_COMBOBOX_SELECT,&CPage1::OnComboBoxSelect);
	XEle_RegEventCPP(m_hComboBox,XE_LBUTTONUP,&CPage1::OnLButtonUp);


	m_hLayoutSlnVer = XC_GetObjectByID(m_hParent,ID_page1_layoutSlnVer);
//	m_hLayoutSlnVer = XC_GetObjectByRes(m_hParent,L"ID_page1_layoutSlnVer");
//	HXCGUI hParentXCGUI = XLayout_GetParent(m_hLayoutSlnVer);
// 	XC_OBJECT_TYPE tp = XC_GetObjectType(hParentXCGUI);
// 
// 	HWINDOW hWindow = XLayout_GetWindow(m_hLayoutSlnVer);
// 	if (hWindow == hParentWindow)
// 	{
// 
// 	}

	//加载难度图片

	wchar_t hImageHardPath[MAX_PATH_XC] = {0};

	int nleft = 0;
	int nRight = 0;
	for (int k = 0; k < 11; k++)
	{
		if (k % 2 == 0)
		{
			wsprintfW(hImageHardPath,L"%s\\%s\\star%d.png",m_pCurDir,XC_PATH_LAYOUTRES_IMAGE_DIR,nleft);
		}else
		{
			wsprintfW(hImageHardPath,L"%s\\%s\\star%d.%d.png",m_pCurDir,XC_PATH_LAYOUTRES_IMAGE_DIR,nleft,nRight);
		}
		nRight+=5;
		if (nRight == 10)
		{
			nleft++;
			nRight=0;
		}
#if _DEBUG
//		OutputDebugStringW(L"\r\n");
//		OutputDebugStringW(hImageHardPath);
#endif

		m_hImageHard[k] = XImage_LoadFile(hImageHardPath);
		XImage_EnableAutoDestroy(m_hImageHard[k],FALSE);
	}

	m_hEditPath = XC_GetHEleByRes(m_hParent,L"ID_editPath");
	XRichEdit_SetText(m_hEditPath,m_pTempDir);



	XEle_PostEvent(m_hComboBox,m_hComboBox,XE_COMBOBOX_SELECT,(WPARAM)m_SaveData.GetSln(),NULL);


	//注册 查找，和分类标签事件


#define OnEventCpp(tWindow,tId,tConstEvent,tCallBack) \
	XEle_RegEventCPP(XC_GetHEleByID(tWindow,tId),tConstEvent,tCallBack);

#define OnEventCpp1(tWindow,tId,tConstEvent,tCallBack) \
	XEle_RegEventCPP1(XC_GetHEleByID(tWindow,tId),tConstEvent,tCallBack);

#define OnEventBnClickCpp1(tWindow,tConstEvent,tCallBack)\
	OnEventCpp1(tWindow,tConstEvent,XE_BNCLICK,tCallBack);
	

#define OnEventBnClickCpp1_(tConstEvent,tCallBack)\
	OnEventCpp1(m_hParent,tConstEvent,XE_BNCLICK,tCallBack);


	OnEventCpp1(m_hParent,ID_page1_btn_showall,XE_BNCLICK,&CPage1::OnShowBtnClick);
	OnEventCpp1(m_hParent,ID_page1_btn_showcomplete,XE_BNCLICK,&CPage1::OnShowBtnClick);
	OnEventCpp1(m_hParent,ID_page1_btn_showele,XE_BNCLICK,&CPage1::OnShowBtnClick);
	OnEventCpp1(m_hParent,ID_page1_btn_showlayout,XE_BNCLICK,&CPage1::OnShowBtnClick);
	OnEventCpp1(m_hParent,ID_page1_btn_showBtn,XE_BNCLICK,&CPage1::OnShowBtnClick);
	OnEventCpp1(m_hParent,ID_page1_btn_showWindow,XE_BNCLICK,&CPage1::OnShowBtnClick);
	OnEventCpp1(m_hParent,ID_page1_btn_listview,XE_BNCLICK,&CPage1::OnShowBtnClick);
	OnEventCpp1(m_hParent,ID_page1_btn_showuserDraw,XE_BNCLICK,&CPage1::OnShowBtnClick);
	OnEventCpp1(m_hParent,ID_page1_btn_listbox,XE_BNCLICK,&CPage1::OnShowBtnClick);
	OnEventCpp1(m_hParent,ID_page1_btn_menu,XE_BNCLICK,&CPage1::OnShowBtnClick);
	OnEventCpp1(m_hParent,ID_page1_btn_list,XE_BNCLICK,&CPage1::OnShowBtnClick);
	OnEventCpp1(m_hParent,ID_page1_btn_tree,XE_BNCLICK,&CPage1::OnShowBtnClick);

//	XEle_RegEventCPP(XC_GetHEleByRes(m_hParent,L"ID_page1_btn_showall"),XE_BNCLICK,&CPage1::OnBtnShowAllClick);
// 	XEle_RegEventCPP1(XC_GetHEleByRes(m_hParent,L"ID_page1_btn_showcomplete"),XE_BNCLICK,&CPage1::OnShowBtnClick);
// 	XEle_RegEventCPP1(XC_GetHEleByRes(m_hParent,L"ID_page1_btn_showele"),XE_BNCLICK,&CPage1::OnShowBtnClick);
// 	XEle_RegEventCPP1(XC_GetHEleByRes(m_hParent,L"ID_page1_btn_showlayout"),XE_BNCLICK,&CPage1::OnShowBtnClick);
// 	XEle_RegEventCPP1(XC_GetHEleByRes(m_hParent,L"ID_page1_btn_showBtn"),XE_BNCLICK,&CPage1::OnShowBtnClick);//显示按钮
// 	XEle_RegEventCPP1(XC_GetHEleByRes(m_hParent,L"ID_page1_btn_showWindow"),XE_BNCLICK,&CPage1::OnShowBtnClick);//显示窗口
// 	XEle_RegEventCPP1(XC_GetHEleByRes(m_hParent,L"ID_page1_btn_listview"),XE_BNCLICK,&CPage1::OnShowBtnClick);
// 	XEle_RegEventCPP1(XC_GetHEleByRes(m_hParent,L"ID_page1_btn_showuserDraw"),XE_BNCLICK,&CPage1::OnShowBtnClick);
// 	XEle_RegEventCPP1(XC_GetHEleByRes(m_hParent,L"ID_page1_btn_listbox"),XE_BNCLICK,&CPage1::OnShowBtnClick);
// 	XEle_RegEventCPP1(XC_GetHEleByRes(m_hParent,L"ID_page1_btn_menu"),XE_BNCLICK,&CPage1::OnShowBtnClick);	
// 	XEle_RegEventCPP1(XC_GetHEleByRes(m_hParent,L"ID_page1_btn_list"),XE_BNCLICK,&CPage1::OnShowBtnClick);
// 	XEle_RegEventCPP1(XC_GetHEleByRes(m_hParent,L"ID_page1_btn_tree"),XE_BNCLICK,&CPage1::OnShowBtnClick);


	m_hEditSearch = (HELE)XC_GetObjectByID(m_hParent,ID_page1_editSearch);
	XRichEdit_SetDefaultText(m_hEditSearch,L"输入关键字 回车搜索 del删除");
	XRichEdit_EnableAutoSelAll(m_hEditSearch,FALSE);
	XEle_RegEventCPP(m_hEditSearch,XE_KEYDOWN,&CPage1::OnEventKeyDown);
}

int CPage1::OnListboxMouseStay( BOOL *pbHandled )
{

	HELE hSbar = XSView_GetScrollBarV(m_hListBox);
	if (XEle_IsShow(hSbar) == FALSE)
	{
		XEle_ShowEle(hSbar,TRUE);
		XEle_RedrawEle(m_hListBox);
	}
// 	XSView_ShowSBarV(m_hListBox,TRUE);
// 	XEle_RedrawEle(m_hListBox,TRUE);
// 	xtrace("In----------\r\n");
	return 0;
}


VOID HideScrollBarV(HELE hListBox)
{
	HELE hSbar = XSView_GetScrollBarV(hListBox);
	if (XEle_IsShow(hSbar))
	{
		XEle_ShowEle(hSbar,FALSE);
		XEle_RedrawEle(hListBox);
	}
}
int CPage1::OnListboxMouseLeave( HELE hEleStay,BOOL *pbHandled )
{



	POINT pt;
	GetCursorPos(&pt);
	ScreenToClient(XEle_GetHWND(m_hListBox),&pt);


	RECT rt;
	XEle_GetClientRect(m_hListBox,&rt);
	XEle_RectClientToWndClient(m_hListBox,&rt);
	if (pt.x >= rt.left && pt.x <= rt.right && pt.y >= rt.top && pt.y <= rt.bottom)
	{
		xtrace("a--------------------------bc\r\n");
		return 0;
	}


	
	if (hEleStay)
	{
		RECT rtStay;
		XEle_GetRect(hEleStay,&rtStay);
		
		RECT rtListBox;
		XEle_GetRect(m_hListBox,&rtListBox);
		
		if (XC_RectInRect(&rtStay,&rtListBox))
		{
			return 0;
		}
	}
	

	HXCGUI hEle = XListBox_GetTemplateObject(m_hListBox,nMouseInItemID,12);
	if (XC_IsHELE(hEle))
	{
		XEle_ShowEle((HELE)hEle,FALSE);
		XEle_RedrawEle(m_hListBox,FALSE);
		m_hLastItemEle = (HELE)hEle;
	}

// 	RECT rt1,rt2;
// 	XEle_GetClientRect(m_hListBox,&rt1);
// 	XEle_RectClientToWndClient(m_hListBox,&rt1);
// 	XEle_GetClientRect(hEleStay,&rt2);
// 	XEle_RectClientToWndClient(hEleStay,&rt2);
// 
// 	if (!XC_RectInRect(&rt1,&rt2) )
// 	{
// 		HideScrollBarV(m_hListBox);
// 		return 0;
// 	}
// 
// 	xtrace("%d-%d-%d-%d|%d-%d-%d-%d\r\n",rt1.left,rt1.top,rt1.right,rt1.bottom,rt2.left,rt2.top,rt2.right,rt2.bottom);
// 	return 0;
	

// 	POINT pt;
// 	GetCursorPos(&pt);
// 	ScreenToClient(XEle_GetHWND(m_hListBox),&pt);
// 
// 	RECT rt;
// 	XEle_GetClientRect(m_hListBox,&rt);
// 	XEle_RectClientToWndClient(m_hListBox,&rt);
// 	if (pt.x < rt.left || pt.x > rt.right || pt.y < rt.top || pt.y > rt.bottom)
// 	{
// 		XSView_ShowSBarV(m_hListBox,FALSE);
// 		XEle_RedrawEle(m_hListBox,TRUE);
// 		return 0;
// 	}
// 
// 	xtrace("%d-%d-%d-%d|%d-%d\r\n",rt.left,rt.top,rt.right,rt.bottom,pt.x,pt.y);

	return 0;


// 	if (hEleStay == NULL)
// 	{
// 		XSView_ShowSBarV(m_hListBox,FALSE);
// 		XEle_RedrawEle(m_hListBox,TRUE);
// 		return 0;
// 	}
// 
// 	if (XC_IsHXCGUI(hEleStay,XC_BUTTON))
// 	{
// 		HELE hEle = XEle_GetParentEle(hEleStay);
// //		XC_OBJECT_TYPE objt = XC_GetObjectType(hEle);
// 		if (XC_IsHXCGUI(hEle,XC_SCROLLBAR))
// 		{
// 			hEle = XEle_GetParentEle(hEle);
// //			XC_OBJECT_TYPE objt = XC_GetObjectType(hEle);
// 
// 			if (XC_IsHXCGUI(hEle,XC_LISTBOX))
// 			{
// 				return 0;
// 			}
// 			
// 		}
// 		
// 	}
// 
// 	HELE hEle = XEle_GetParentEle(hEleStay);
// 	if (hEle != m_hListBox)
// 	{
// 		XSView_ShowSBarV(m_hListBox,FALSE);
// 		XEle_RedrawEle(m_hListBox,TRUE);
// 		
// 		xtrace("Out----------\r\n");
// 	}else
// 	{
// 		POINT pt;
// 		GetCursorPos(&pt);
// 		ScreenToClient(XEle_GetHWND(m_hListBox),&pt);
// 		XEle_PointWndClientToEleClient(hEle,&pt);
// 
// 		if (XListBox_HitTestOffset(hEle,&pt) == -1)
// 		{
// 			XSView_ShowSBarV(m_hListBox,FALSE);
// 			XEle_RedrawEle(m_hListBox,TRUE);
// 		}
// 		
// 		
// 		xtrace("Out----%d------\r\n",XC_GetObjectType(hEle));
// 	}
// 
// 	
// 
// 	return 0;
}


int  CPage1::OnShowBtnClick(HELE hEventEle,BOOL *pbHandled)
{
	wchar_t buffer[MAX_PATH_XC] = {0};
	XBtn_GetText(hEventEle,buffer,MAX_PATH_XC);

	if (wcscmp(buffer,L"全部")==0)
		ShowCategory(NULL);
	else
		ShowCategory(buffer);
	return 0;
}

void CPage1::GetSetFileFullPath( int nIndex,wchar_t* pBuffer )
{
	wchar_t szPrjName[MAX_PATH_XC] ={0};
	XAdapterTable_GetItemText(m_hAdapterListBox,nIndex,0,szPrjName,MAX_PATH);
	
	//然后。组合代码目录		
	wsprintfW(pBuffer,L"%s\\%s\\%s\\%s\\%s",
		m_pDemoMgrDir,m_language,XC_DIRNAME_CODECPP,szPrjName,L"Set.file");
#ifdef _DEBUG
	xtracew(pBuffer);
#endif
}



BOOL CPage1::IsFileExist( int nSelect ,const wchar_t* pFileName )
{
	wchar_t szPrjName[MAX_PATH_XC] ={0};
	XAdapterTable_GetItemText(m_hAdapterListBox,nSelect,0,szPrjName,MAX_PATH);
	
	//然后。组合代码目录
	wchar_t SrcPath[MAX_PATH_XC] = {0};
	
	wsprintfW(SrcPath,L"%s\\%s\\%s\\%s\\%s",
		m_pDemoMgrDir,m_language,XC_DIRNAME_CODECPP,szPrjName,pFileName);
	
	//		OutputDebugStringW(SrcPath);
	
	return XC_IsFileExsit(SrcPath);
}



void CPage1::ShowCategory( const wchar_t* lpCategory )
{
	HXCGUI hAdapter= XListBox_GetAdapter(m_hListBox);
	//第一步，枚举工程文件
	XAdapterTable_DeleteItemAll(m_hAdapterListBox);	
	
	
	//组合成ide版本目录
	wchar_t strCodeDir[4096] = {0};
	wsprintfW(strCodeDir,L"%s\\%s\\%s",m_pDemoMgrDir,m_language,XC_DIRNAME_CODECPP);
	//枚举
	CEnumPath Enum(strCodeDir);
	
	//如果是全部显示
	if (lpCategory == NULL)
	{
		for (int i = 0; i < Enum.GetItemCount(); i++)
		{
			XAdapterTable_AddItemText(m_hAdapterListBox,Enum[i]);
		}
		
	}else
	{
		for (int i = 0; i < Enum.GetItemCount(); i++)
		{
			if (StrStrIW(Enum[i],lpCategory))
			{
				XAdapterTable_AddItemText(m_hAdapterListBox,Enum[i]);
			}
		}
		
	}
	
	XEle_RedrawEle(m_hListBox);
}


int CPage1::OnEventKeyDown(WPARAM wParam,LPARAM lParam,BOOL *pbHandled)
{
	if (wParam == VK_RETURN)
	{
		wchar_t buffer[MAX_PATH_XC] = {0};
		XRichEdit_GetText(m_hEditSearch,buffer,MAX_PATH_XC);
		if (wcslen(buffer) == 0)
		{
			ShowCategory(NULL);
		}else
		{
			ShowCategory(buffer);
		}
		*pbHandled = TRUE;
	}else if (wParam == VK_DELETE)
	{
		XRichEdit_SetText(m_hEditSearch,L"");
		*pbHandled  = TRUE;
	}
	return 0;
}

int CPage1::OnListBoxTemplateCreateEnd( listBox_item_i* pItem,BOOL *pbHandled )
{
	HELE hBtnViewCode = (HELE)XListBox_GetTemplateObject(m_hListBox,pItem->index,2);
	if (XC_GetObjectType(hBtnViewCode) == XC_BUTTON)
	{
		XEle_RegEventCPP1(hBtnViewCode,XE_BNCLICK,&CPage1::OnBtnViewCodeClick);
	}
	
	HELE hBtnCreate = (HELE)XListBox_GetTemplateObject(m_hListBox,pItem->index,3);
	if (XC_GetObjectType(hBtnCreate) == XC_BUTTON)
	{
		XEle_RegEventCPP1(hBtnCreate,XE_BNCLICK,&CPage1::OnBtnCreateCodeClick);
	}

	HELE hBtnViewPic = (HELE)XListBox_GetTemplateObject(m_hListBox,pItem->index,4);
	if (XC_GetObjectType(hBtnViewPic) == XC_BUTTON)
	{
		if (IsFileExist(pItem->index,L"Demo.png") ||
			IsFileExist(pItem->index,L"Demo.gif") || 
			IsFileExist(pItem->index,L"Demo.jpg"))
		{
			XEle_RegEventCPP1(hBtnViewPic,XE_BNCLICK,&CPage1::OnBtnViewPicClick);
		}else
		{
			XEle_ShowEle(hBtnViewPic,FALSE);
		}
		
	}

	CMemSetFile m_MemSetFile;
	wchar_t setFileBuffer[4096] = {0};
	GetSetFileFullPath(pItem->index,setFileBuffer);
	BOOL bOk = FALSE;
	if (XC_IsFileExsit(setFileBuffer))
	{
		bOk = m_MemSetFile.Init(setFileBuffer);
#ifdef _DEBUG
		if (!bOk)
		{
			xtrace("加载配置文件失败!\r\n");
		}
#endif
	}
	
	HXCGUI hPicImage = (HELE)XListBox_GetTemplateObject(m_hListBox,pItem->index,6);
	if (XC_GetObjectType(hPicImage) == XC_SHAPE_PICTURE)
	{
		if (bOk)
		{
			int nStarCount = m_MemSetFile.GetDataInt(L"Star",L"Count");
			XShapePic_SetImage(hPicImage,m_hImageHard[nStarCount]);
		} 
		else
		{
			XShapePic_SetImage(hPicImage,m_hImageHard[1]);
		}

	}

	HXCGUI hShapeTextAuthor = XListBox_GetTemplateObject(m_hListBox,pItem->index,7);
	if (XC_IsHXCGUI(hShapeTextAuthor,XC_SHAPE_TEXT))
	{
		if (bOk)
		{
			const wchar_t* pStrAuthor = m_MemSetFile.GetDataStr(L"Author",L"Name");
			if (pStrAuthor != NULL)
			{
				XShapeText_SetText(hShapeTextAuthor,pStrAuthor);
			}
		}
	}

//	xtrace("模板创建完毕\r\n");
	*pbHandled = TRUE;
	return 0;
}

int CALLBACK OnWndLButtonDBClick(HWINDOW hWindow,UINT nFlags,POINT *pPt,BOOL *pbHandled)
{
	PostMessageA(XWnd_GetHWND(hWindow),WM_CLOSE,0,0);
	return 0;
}

int CPage1::OnBtnViewPicClick( HELE hEventEle,BOOL* pbHandled )
{
	int nSelect = XListBox_GetItemIndexFromHXCGUI(m_hListBox,hEventEle);
	XListBox_SetSelectItem(m_hListBox,nSelect);
	XListBox_RefreshData(m_hListBox);

	//判断文件类型，如果是gif就加载gif窗口，
	wchar_t szPrjName[MAX_PATH_XC] ={0};
	XAdapterTable_GetItemText(m_hAdapterListBox,nSelect,0,szPrjName,MAX_PATH_XC);
	


	HWINDOW hWindowDemoPic = NULL;
	HIMAGE hImage = NULL;
	if (IsFileExist(nSelect,L"Demo.gif"))
	{
		wchar_t WindowDemoPicPath[MAX_PATH_XC] = {0};
		wsprintfW(WindowDemoPicPath,L"%s\\%s",
			m_pCurDir,XC_FULLPATH_LAYOUT_PAGE1_DEMOGIF);

		wchar_t DemoPic[MAX_PATH_XC] = {0};
		wsprintfW(DemoPic,L"%s\\%s\\%s\\%s\\%s",
			m_pDemoMgrDir,m_language,XC_DIRNAME_CODECPP,szPrjName,L"Demo.gif");

		hWindowDemoPic = (HWINDOW)XC_LoadLayout(WindowDemoPicPath,m_hParent);
		HXCGUI hShapeGif = XC_GetShapeByRes(hWindowDemoPic,L"ID_DemoGif");
		hImage = XImage_LoadFile(DemoPic);
		XShapeGif_SetImage(hShapeGif,hImage);

	}
	else if (IsFileExist(nSelect,L"Demo.png"))
	{

		wchar_t WindowDemoPicPath[MAX_PATH_XC] = {0};
		wsprintfW(WindowDemoPicPath,L"%s\\%s",
			m_pCurDir,XC_FULLPATH_LAYOUT_PAGE1_DEMOPIC);
	
		wchar_t DemoPic[MAX_PATH_XC] = {0};
		wsprintfW(DemoPic,L"%s\\%s\\%s\\%s\\%s",
			m_pDemoMgrDir,m_language,XC_DIRNAME_CODECPP,szPrjName,L"Demo.png");
		
		hWindowDemoPic = (HWINDOW)XC_LoadLayout(WindowDemoPicPath,m_hParent);
		HXCGUI hShapePic = XC_GetShapeByRes(hWindowDemoPic,L"ID_DemoPic");
		hImage = XImage_LoadFile(DemoPic);
		XShapePic_SetImage(hShapePic,hImage);
		

	}else if (IsFileExist(nSelect,L"Demo.jpg"))
	{
		wchar_t WindowDemoPicPath[MAX_PATH_XC] = {0};
		wsprintfW(WindowDemoPicPath,L"%s\\%s",
			m_pCurDir,XC_FULLPATH_LAYOUT_PAGE1_DEMOPIC);
			
		wchar_t DemoPic[MAX_PATH_XC] = {0};
		wsprintfW(DemoPic,L"%s\\%s\\%s\\%s\\%s",
			m_pDemoMgrDir,m_language,XC_DIRNAME_CODECPP,szPrjName,L"Demo.jpg");
				
		hWindowDemoPic = (HWINDOW)XC_LoadLayout(WindowDemoPicPath,m_hParent);
		HXCGUI hShapePic = XC_GetShapeByRes(hWindowDemoPic,L"ID_DemoPic");
		hImage = XImage_LoadFile(DemoPic);
		XShapePic_SetImage(hShapePic,hImage);
		
	}else
	{
		return 0;
	}
	//保存预览窗口句柄
	if (m_hWindowPicView)
	{
		RECT rect;
		XWnd_GetClientRect(hWindowDemoPic,&rect);
		rect.right = rect.left + XImage_GetWidth(hImage) +16;
		rect.bottom= rect.top  + XImage_GetHeight(hImage)+18;

		::MoveWindow(XWnd_GetHWND(hWindowDemoPic),m_RectPicWindow.left,
			m_RectPicWindow.top,rect.right - rect.left,
			rect.bottom - rect.top,FALSE);
		
		XWnd_CloseWindow(m_hWindowPicView);
	}else
	{

		HWND hWnd = XWnd_GetHWND(hWindowDemoPic);
		//居中窗口
		int scrWidth, scrHeight;
		RECT rect;
		
		scrWidth = GetSystemMetrics(SM_CXSCREEN);
		scrHeight = GetSystemMetrics(SM_CYSCREEN);
		XWnd_GetClientRect(hWindowDemoPic,&rect);
		//		GetWindowRect(hWnd, &rect);
		rect.right = rect.left + XImage_GetWidth(hImage) +16;
		rect.bottom= rect.top  + XImage_GetHeight(hImage)+18;
		
		SetWindowPos(hWnd, HWND_TOP, 
			(scrWidth - rect.right) / 2,
			(scrHeight - rect.bottom) / 2,
			rect.right - rect.left,
			rect.bottom - rect.top, SWP_SHOWWINDOW);
		
	}
	
	m_hWindowPicView = hWindowDemoPic;
	XWnd_RegEventC1(hWindowDemoPic,WM_LBUTTONDBLCLK,OnWndLButtonDBClick);
	XWnd_RegEventCPP1(m_hWindowPicView,WM_DESTROY,&CPage1::OnWndDestroy);
	XWnd_RegEventCPP1(m_hWindowPicView,WM_MOVE,&CPage1::OnPicViewWndMove);
	XWnd_AdjustLayout(hWindowDemoPic);
	XWnd_ShowWindow(hWindowDemoPic,SW_SHOW);


	return 0;
}


int CPage1::OnWndDestroy(HWINDOW hWindow,BOOL *pbHandled )
{
	m_hWindowPicView = NULL;
	return 0;
}


int CPage1::OnPicViewWndMove(HWINDOW hWindow,WPARAM wParam,LPARAM lParam,BOOL *pbHandled )
{
	GetWindowRect(XWnd_GetHWND(hWindow),&m_RectPicWindow);
	return 0;
}


int CPage1::OnBtnCreateCodeClick( HELE hEventEle,BOOL* pbHandled )
{
	

	//得到目录名称
	int nSelect = XListBox_GetItemIndexFromHXCGUI(m_hListBox,hEventEle);
	XListBox_SetSelectItem(m_hListBox,nSelect);
	XListBox_RefreshData(m_hListBox);

	wchar_t szPrjName[MAX_PATH_XC] ={0};
	XAdapterTable_GetItemText(m_hAdapterListBox,nSelect,0,szPrjName,MAX_PATH_XC);
	
	wchar_t m_strSlnVer[MAX_PATH_XC] = {0}; //工程版本
	XBtn_GetText(m_hBtnSlnVer,m_strSlnVer,MAX_PATH_XC);


	//第1步，把Cpp工程文件先复制到临时目录
	wchar_t SrcPath[MAX_PATH_XC] = {0};
	wsprintfW(SrcPath,L"%s\\%s\\%s\\%s",
		m_pDemoMgrDir,m_language,XC_DIRNAME_CODECPP,szPrjName);
	
	if (XC_CopyFile(SrcPath,m_pTempDir))
	{
		MessageBoxW(NULL,SrcPath,L"复制目录失败",MB_OK);
		return 0;
	}

	//第2步 把Sln里面的文件复制过去

	{
		wchar_t slnPath[MAX_PATH_XC] = {0};
		wsprintfW(slnPath,L"%s\\%s\\%s\\%s",m_pDemoMgrDir,m_language,XC_DIRNAME_CODESLN,m_strSlnVer);
		
		OutputDebugStringW(slnPath);

		CEnumPath EnumFullPath(slnPath,FALSE,TRUE);
		CEnumPath EnumName(slnPath,FALSE);
		for (int k = 0; k < EnumName.GetItemCount(); k++)
		{
			wchar_t newFile[MAX_PATH_XC] = {0};
			wsprintfW(newFile,L"%s\\%s\\%s",m_pTempDir,szPrjName,EnumName[k]);
#ifdef _DEBUG
			OutputDebugStringW(L"\r\n");
			OutputDebugStringW(newFile);
			OutputDebugStringW(L"\r\n");
			OutputDebugStringW(EnumFullPath[k]);
#endif

			if (!XC_IsFileExsit(newFile))
			{
				if (!CopyFileW(EnumFullPath[k],newFile,FALSE))
				{
					MessageBoxW(NULL,EnumName[k],L"复制文件失败",MB_OK);
				}
			}

		}

		//把sln里面的子目录拷贝过去
		CEnumPath EnumDirFullPath(slnPath,TRUE,TRUE);
		CEnumPath EnumDirName(slnPath);
		for (int p = 0; p < EnumDirName.GetItemCount(); p++)
		{
			wchar_t newDirPath[MAX_PATH_XC] = {0};
//			wsprintfW(newDirPath,L"%s\\%s\\%s",m_pTempDir,szPrjName,EnumDirName[p]);
			wsprintfW(newDirPath,L"%s\\%s",m_pTempDir,szPrjName);
#ifdef _DEBUG
			OutputDebugStringW(L"\r\n");
			OutputDebugStringW(newDirPath);
			OutputDebugStringW(L"\r\n");
			OutputDebugStringW(EnumFullPath[p]);
#endif


			if (XC_CopyFile(EnumDirFullPath[p],newDirPath))
			{
				MessageBoxW(NULL,SrcPath,L"复制目录失败",MB_OK);
				return 0;
			}
		}



	}



	
	wchar_t LibPath[MAX_PATH_XC] = {0};
	wsprintfW(LibPath,L"%s\\%s\\%s",m_pDemoMgrDir,m_language,XC_DIRNAME_CODELIB);
	
	//将lib目录的文件复制到生成目录
	CEnumPath EnumFullPath(LibPath,FALSE,TRUE);
	CEnumPath EnumName(LibPath,FALSE);
	for (int k = 0; k < EnumName.GetItemCount(); k++)
	{
		wchar_t newFile[MAX_PATH_XC] = {0};
		wsprintfW(newFile,L"%s\\%s\\%s",m_pTempDir,szPrjName,EnumName[k]);
		if (!CopyFileW(EnumFullPath[k],newFile,FALSE))
		{
			MessageBoxW(NULL,EnumName[k],L"复制lib文件失败",MB_OK);
		}
	}

	//将lib目录的文件夹复制到生成目录

	CEnumPath EnumDir(LibPath,TRUE,TRUE);
	CEnumPath EnumDirName(LibPath,TRUE);
	for (int x = 0; x < EnumDir.GetItemCount(); x++)
	{
		wchar_t newFile[MAX_PATH_XC] = {0};
		wsprintfW(newFile,L"%s\\%s\\%s",m_pTempDir,szPrjName,EnumDirName[x]);
		if (XC_CopyFile(EnumDir[x],newFile))
		{
			MessageBoxW(NULL,EnumDir[x],L"复制lib文件夹失败",MB_OK);
		}
	}


	//复制 所有的公用文件  xcgui目录下面的 xcgui.dll等到生成的目录
	wchar_t XcguiLibPath[MAX_PATH_XC] = {0};
	wsprintfW(XcguiLibPath,L"%s\\%s",m_pCurDir,XC_PATH_XCGUILIB);
	CEnumPath EnumXcguiLibFullPath(XcguiLibPath,FALSE,TRUE);
	CEnumPath EnumXcguiLibName(XcguiLibPath,FALSE);
	for (int m = 0; m < EnumXcguiLibName.GetItemCount(); m++)
	{
		wchar_t newFile[MAX_PATH_XC] = {0};
		wsprintfW(newFile,L"%s\\%s\\%s",m_pTempDir,szPrjName,EnumXcguiLibName[m]);
		
		if (!CopyFileW(EnumXcguiLibFullPath[m],newFile,FALSE))
		{
			MessageBoxW(NULL,EnumXcguiLibName[m],L"复制lib文件失败",MB_OK);
		}
	}





	wchar_t OldProjectName[MAX_PATH_XC]={0};
	wchar_t newProjectName[MAX_PATH_XC]={0};
	wsprintfW(OldProjectName,L"%s\\%s",m_pTempDir,szPrjName);
	wsprintfW(newProjectName,L"%s\\%s_%s_%s",m_pTempDir,m_language,m_strSlnVer,szPrjName);

#ifdef _DEBUG
	OutputDebugStringW(L"\r\n");
	OutputDebugStringW(OldProjectName);
	OutputDebugStringW(L"\r\n");
	OutputDebugStringW(newProjectName);
#endif
	


	if (!PathFileExistsW(newProjectName))
	{
		if (!XC_ReNameFolder(OldProjectName,newProjectName))
		{
			MessageBoxW(NULL,newProjectName,L"重命名失败!",MB_OK);
		}
	}else
	{
		//如果存在，就删除临时生成的
		XC_DeleteFolder(OldProjectName);
	}
	//打开生成的文件目录
	XC_OpenFloder(newProjectName);
	
	return 0;
}

int CPage1::OnBtnViewCodeClick( HELE hEventEle,BOOL* pbHandled )
{

	//获取例子工程目录名称
	int nSelect = XListBox_GetItemIndexFromHXCGUI(m_hListBox,hEventEle);
	XListBox_SetSelectItem(m_hListBox,nSelect);
	XListBox_RefreshData(m_hListBox);

	wchar_t szPrjName[MAX_PATH_XC] ={0};
	XAdapterTable_GetItemText(m_hAdapterListBox,nSelect,0,szPrjName,MAX_PATH_XC);
	

	//然后。组合代码目录
	wchar_t SrcPath[4096] = {0};
	wchar_t DesPath[4096] = {0};
	
	wsprintfW(SrcPath,L"%s\\%s\\%s\\%s",
		m_pDemoMgrDir,m_language,XC_DIRNAME_CODECPP,szPrjName);
	
	XC_OpenFloder(SrcPath);
	return 0;
}

HELE CPage1::CreateGrounpButton( wchar_t* pbuttonName,HXCGUI hParent,int nGrounpID )
{
	HELE hBtn = XBtn_Create(0,0,50,30,pbuttonName,hParent);
	XLayout_AddEle(m_hLayoutSlnVer,hBtn);
	XBtn_SetType(hBtn,button_type_radio);
	XBtn_SetStyle(hBtn,button_style_radio);
	XBtn_SetTextAlign(hBtn,textAlignFlag_left|textAlignFlag_vcenter);
	XBtn_SetGroupID(hBtn,nGrounpID);
	XBtn_SetStyle(hBtn,button_style_default);
	XEle_SetLayoutWidth(hBtn,layout_size_type_fill,0);
	XEle_EnableDrawFocus(hBtn,FALSE);
	
	return hBtn;
}







///////////////////////////////////////////////////////

CPage2::CPage2()
{

	ZeroMaxPathXC(m_TechPath);
	wsprintfW(m_TechPath,L"%s\\%s\\%s",GetAppDir(),XC_PATH_ROOT,XC_DIRNAME_TECH);
	CreateDirectoryW(m_TechPath,NULL);

}




VOID CPage2::Create(HWINDOW hParentWindow)
{
	m_hParentWindow = hParentWindow;
	m_hListBox = (HELE)XC_GetObjectByID(m_hParentWindow,ID_page2_listbox);


	{   //初始化listbox相关
		m_hAdapterTableListBox = XAdapterTable_Create();
		XListBox_BindAdapter(m_hListBox,m_hAdapterTableListBox);
		XAdapterTable_AddColumn(m_hAdapterTableListBox,L"name");

		wchar_t lpListBoxTemplateBuffer[MAX_PATH_XC] = {0};
		wsprintfW(lpListBoxTemplateBuffer,L"%s\\%s",GetAppDir(),XC_FULLPATH_LAYOUT_PAGE2_LISTBOX);
		XListBox_SetItemTemplateXML(m_hListBox,lpListBoxTemplateBuffer);
		XListBox_SetItemHeightDefault(m_hListBox,40,40);

		XEle_RegEventCPP(m_hListBox,XE_LISTBOX_TEMP_CREATE_END,&CPage2::OnListBoxTemplateCreateEnd);

		//这里就需要枚举目录然后把所有的东西都枚举到列表框里面
		//第一步，枚举分类语言
		m_hComboBox = (HELE)XC_GetObjectByID(m_hParentWindow,ID_page2_combobox_lang);
		wchar_t lpComboBoxTemplateBuffer[MAX_PATH_XC]={0};
		wsprintfW(lpComboBoxTemplateBuffer,L"%s\\%s",GetAppDir(),XC_FULLPATH_LAYOUT_PAGE2_COMBOBOX);
		XComboBox_SetItemTemplateXML(m_hComboBox,lpComboBoxTemplateBuffer);

		m_hAdapterTableComboBox = XAdapterTable_Create();
		XComboBox_BindApapter(m_hComboBox,m_hAdapterTableComboBox);
		XAdapterTable_AddColumn(m_hAdapterTableComboBox,L"name");

		//注册选择事件，选中以后刷新列表
		XEle_RegEventCPP(m_hComboBox,XE_COMBOBOX_SELECT,&CPage2::OnComboBoxSelect);

		//点击下拉组合框就弹出下拉列表
		XEle_RegEventCPP(m_hComboBox,XE_LBUTTONUP,&CPage2::OnLButtonUp);

		//枚举techmgr目录下面的文件夹添加到列表框
		CEnumPath EnumDir(m_TechPath);
		for (int i = 0; i < EnumDir.GetItemCount(); i++)
		{
			XAdapterTable_AddItemText(m_hAdapterTableComboBox,EnumDir[i]);
		}

		//读取默认配置文件，然后模拟选中某个
		int nDefualtSelect = m_SaveData.GetPage2LangIndex();
		if (nDefualtSelect <= EnumDir.GetItemCount())
		{	
			XEle_PostEvent(m_hComboBox,m_hComboBox,XE_COMBOBOX_SELECT,nDefualtSelect,0);
		}


	}

	m_hRichEdit = (HELE)XC_GetObjectByID(m_hParentWindow,ID_page2_editserch);
	XRichEdit_SetDefaultText(m_hRichEdit,L"输入关键字 回车搜索");

	XEle_RegEventCPP(m_hRichEdit,XE_KEYDOWN,&CPage2::OnEventKeyDown);


	HXCGUI hLayout1 = XC_GetObjectByID(m_hParentWindow,ID_page2_layout1);
	HXCGUI hLayout2 = XC_GetObjectByID(m_hParentWindow,ID_page2_layout2);
	HXCGUI hLayout3 = XC_GetObjectByID(m_hParentWindow,ID_page2_layout3);

	for (int i = 0; i < XLayout_GetChildCount(hLayout1); i++)
	{
		HELE hBtn = (HELE)XLayout_GetChild(hLayout1,i);
		XEle_RegEventCPP1(hBtn,XE_BNCLICK,&CPage2::OnCategoryBtnClick);
	}
	for (int j = 0; j < XLayout_GetChildCount(hLayout2); j++)
	{
		HELE hBtn = (HELE)XLayout_GetChild(hLayout2,j);
		XEle_RegEventCPP1(hBtn,XE_BNCLICK,&CPage2::OnCategoryBtnClick);
	}
	for (int k = 0; k < XLayout_GetChildCount(hLayout3); k++)
	{
		HELE hBtn = (HELE)XLayout_GetChild(hLayout3,k);
		XEle_RegEventCPP1(hBtn,XE_BNCLICK,&CPage2::OnCategoryBtnClick);
	}
}

int CPage2::OnLButtonUp( UINT nFlags, POINT *pPt,BOOL *pbHandled )
{
	RECT rt;
	XComboBox_GetButtonRect(m_hComboBox,&rt);
	
	if (pPt->x >= rt.left && pPt->x  <= rt.right && pPt->y >= rt.top && pPt->y <= rt.bottom)
	{
	}else
	{
		rt.left += 1;
		rt.top  += 1;
		XEle_SendEvent(m_hComboBox,m_hComboBox,XE_LBUTTONDOWN,nFlags, (LPARAM)&rt);
	}
	return 0;
}

int CPage2::OnComboBoxSelect( int iItem,BOOL *pbHandled )
{
	XAdapterTable_DeleteItemAll(m_hAdapterTableListBox);

	//获取语言分类
	wchar_t lpLangName[MAX_PATH_XC]={0};
	XAdapterTable_GetItemText(m_hAdapterTableComboBox,iItem,0,lpLangName,MAX_PATH_XC);


	wchar_t buffer[MAX_PATH_XC]={0};
	wsprintfW(buffer,L"%s\\%s",m_TechPath,lpLangName);

	//枚举语言下面的教程目录
	CEnumPath EnumDir(buffer);

	for (int i = 0; i < EnumDir.GetItemCount(); i++)
	{
		XAdapterTable_AddItemText(m_hAdapterTableListBox,EnumDir[i]);
	}
	XEle_RedrawEle(m_hListBox);


	m_SaveData.SetPage2LangIndex(iItem);
	m_SaveData.Save();

	return 0;
}

int CPage2::OnListBoxTemplateCreateEnd( listBox_item_i* pItem,BOOL *pbHandled )
{
	HXCGUI hButton = XListBox_GetTemplateObject(m_hListBox,pItem->index,3);
	if (XC_IsHXCGUI(hButton,XC_BUTTON))
	{
		XEle_RegEventCPP1((HELE)hButton,XE_BNCLICK,&CPage2::OnBtnClick);
	}
	*pbHandled = TRUE;
	return 0;
}

int CPage2::OnBtnClick(HELE hEventEle,BOOL *pbHandled )
{
	//语言种类
	wchar_t lpLang[MAX_PATH_XC]={0};
	XRichEdit_GetText(m_hComboBox,lpLang,MAX_PATH_XC);

	//目录名称
	wchar_t lpTechFileName[MAX_PATH_XC]={0};
	int nIndex = XListBox_GetItemIndexFromHXCGUI(m_hListBox,hEventEle);
	XAdapterTable_GetItemText(m_hAdapterTableListBox,nIndex,0,lpTechFileName,MAX_PATH_XC);

	wchar_t lpTechFileDir[MAX_PATH_XC]={0};
	wsprintfW(lpTechFileDir,L"%s\\%s\\%s",m_TechPath,lpLang,lpTechFileName);

	XC_OpenFloder(lpTechFileDir);

	*pbHandled = TRUE;
	return 0;
}

int CPage2::OnCategoryBtnClick(HELE hEventEle,BOOL *pbHandled )
{
	wchar_t buffer[MAX_PATH_XC]={0};
	XBtn_GetText(hEventEle,buffer,MAX_PATH_XC);

	if (wcscmp(buffer,L"全部")==0)
	{
		ShowCategory(NULL);
	} 
	else
	{
		ShowCategory(buffer);
	}

	

	return 0;
}

void CPage2::ShowCategory( const wchar_t* lpCategory )
{
	XAdapterTable_DeleteItemAll(m_hAdapterTableListBox);
	
	
	
	wchar_t langBuffer[MAX_PATH_XC]={0};
	XRichEdit_GetText(m_hComboBox,langBuffer,MAX_PATH_XC);
	
	wchar_t pathBuffer[MAX_PATH_XC]={0};
	wsprintfW(pathBuffer,L"%s\\%s",m_TechPath,langBuffer);
	
	
	//枚举目录名称，然后找到关键字相关的显示
	CEnumPath enumDir(pathBuffer);
	
	if (lpCategory == NULL)
	{
		for (int i = 0; i < enumDir.GetItemCount(); i++)
		{
			XAdapterTable_AddItemText(m_hAdapterTableListBox,enumDir[i]);
		}
		
	} 
	else
	{
		for (int i = 0; i < enumDir.GetItemCount(); i++)
		{
			if (StrStrIW(enumDir[i],lpCategory))
			{
				XAdapterTable_AddItemText(m_hAdapterTableListBox,enumDir[i]);
			}
		}
		
		
	}

	XEle_RedrawEle(m_hListBox);
}

int CPage2::OnEventKeyDown( WPARAM wParam,LPARAM lParam,BOOL *pbHandled )
{
	if (wParam == VK_RETURN)
	{
		wchar_t buffer[MAX_PATH_XC] = {0};
		XRichEdit_GetText(m_hRichEdit,buffer,MAX_PATH_XC);
		
		if (wcslen(buffer) == 0)
		{
			ShowCategory(NULL);
		}else
		{
			ShowCategory(buffer);
		}
	}
	return 0;
}


/////////////////////////////////////////////////

CPage3::CPage3()
{
	ZeroMaxPathXC(m_pLuaUIMgrDir);
	wsprintfW(m_pLuaUIMgrDir,L"%s\\%s",GetAppDir(),XC_PATH_LUAUI);
	CreateDirectoryW(m_pLuaUIMgrDir,NULL);	
}

VOID CPage3::Create(HWINDOW hParentWindow)
{
	m_hParentWindow = hParentWindow;
	m_hListBox   = (HELE)XC_GetObjectByID(m_hParentWindow,ID_page3_listbox);
	m_hComboBox  = (HELE)XC_GetObjectByID(m_hParentWindow,ID_page3_combobox);

	//组合目录，设置Combobox 脚本分类
	wchar_t ComboBoxXmlFile[MAX_PATH_XC] = {0};
	wsprintfW(ComboBoxXmlFile,L"%s\\%s",GetAppDir(),XC_FULLPATH_LAYOUT_PAGE3_COMBOBOX);
	XComboBox_SetItemTemplateXML(m_hComboBox,ComboBoxXmlFile);

	m_hAdapterComboBox = XAdapterTable_Create();
	XComboBox_BindApapter(m_hComboBox,m_hAdapterComboBox);
	XAdapterTable_AddColumn(m_hAdapterComboBox,L"name");

	//注册选择事件，选中以后刷新列表
	XEle_RegEventCPP(m_hComboBox,XE_COMBOBOX_SELECT,&CPage3::OnComboBoxSelect);
	XEle_RegEventCPP(m_hComboBox,XE_LBUTTONUP,&CPage3::OnComboBoxLButtonUp);

	//枚举目录，把分类插进去
	CEnumPath EnumDir(m_pLuaUIMgrDir);
	for (int i = 0; i < EnumDir.GetItemCount(); i++)
	{
		XAdapterTable_AddItemText(m_hAdapterComboBox,EnumDir[i]);
	}


	//组合目录，设置listbox的模板
	wchar_t listboxTemplateFilePath[MAX_PATH_XC]={0};
	wsprintfW(listboxTemplateFilePath,L"%s\\%s",GetAppDir(),XC_FULLPATH_LAYOUT_PAGE3_LISTBOX);
	XListBox_SetItemTemplateXML(m_hListBox,listboxTemplateFilePath);
	XListBox_SetItemHeightDefault(m_hListBox,40,40);

	XEle_RegEventCPP(m_hListBox,XE_LISTBOX_TEMP_CREATE_END,&CPage3::OnListBoxTemplateCreateEnd);

	m_hAdapterTableListBox = XAdapterTable_Create();
	XListBox_BindAdapter(m_hListBox,m_hAdapterTableListBox);
	XAdapterTable_AddColumn(m_hAdapterTableListBox,L"name");


	//读取默认配置文件，然后模拟选中某个
	int nDefualtSelect = m_SaveData.GetPage3LangIndex();
	if (nDefualtSelect <= EnumDir.GetItemCount())
	{	
		XEle_PostEvent(m_hComboBox,m_hComboBox,XE_COMBOBOX_SELECT,nDefualtSelect,0);
	}




	m_hRichEdit  = (HELE)XC_GetObjectByID(m_hParentWindow,ID_page3_editserch);
	XRichEdit_SetDefaultText(m_hRichEdit,L"请输入关键字 回车搜索");
	XEle_RegEventCPP(m_hRichEdit,XE_KEYDOWN,&CPage3::OnEventKeyDown);


	HXCGUI hLayout1 = XC_GetObjectByID(hParentWindow,ID_page3_layout1);
	HXCGUI hLayout2 = XC_GetObjectByID(hParentWindow,ID_page3_layou2);
	HXCGUI hLayout3 = XC_GetObjectByID(hParentWindow,ID_page3_layout3);

	{

	for (int i = 0; i < XLayout_GetChildCount(hLayout1); i++)
	{
		HELE hBtn = (HELE)XLayout_GetChild(hLayout1,i);
		XEle_RegEventCPP1(hBtn,XE_BNCLICK,&CPage3::OnBtnCategoryClick);
	}

	for (int j = 0; j < XLayout_GetChildCount(hLayout2); j++)
	{
		HELE hBtn = (HELE)XLayout_GetChild(hLayout2,j);
		XEle_RegEventCPP1(hBtn,XE_BNCLICK,&CPage3::OnBtnCategoryClick);
	}

	for (int k = 0; k < XLayout_GetChildCount(hLayout3); k++)
	{
		HELE hBtn = (HELE)XLayout_GetChild(hLayout3,k);
		XEle_RegEventCPP1(hBtn,XE_BNCLICK,&CPage3::OnBtnCategoryClick);
	}

	}

}


///event///////////////////////////////////////////////////////////////////////

int CPage3::OnComboBoxLButtonUp( UINT nFlags, POINT *pPt,BOOL *pbHandled )
{
	RECT rt;
	XComboBox_GetButtonRect(m_hComboBox,&rt);
	
	if (pPt->x >= rt.left && pPt->x  <= rt.right && pPt->y >= rt.top && pPt->y <= rt.bottom)
	{
	}else
	{
		rt.left += 1;
		rt.top  += 1;
		XEle_SendEvent(m_hComboBox,m_hComboBox,XE_LBUTTONDOWN,nFlags, (LPARAM)&rt);
	}
	return 0;
}


int CPage3::OnComboBoxSelect( int iItem,BOOL *pbHandled )
{
	XAdapterTable_DeleteItemAll(m_hAdapterTableListBox);
	
	//获取语言分类
	wchar_t lpLangName[MAX_PATH_XC]={0};
	XAdapterTable_GetItemText(m_hAdapterComboBox,iItem,0,lpLangName,MAX_PATH_XC);
	
	
	wchar_t buffer[MAX_PATH_XC]={0};
	wsprintfW(buffer,L"%s\\%s",m_pLuaUIMgrDir,lpLangName);
	
	//枚举语言下面的教程目录
	CEnumPath EnumDir(buffer);
	
	for (int i = 0; i < EnumDir.GetItemCount(); i++)
	{
		XAdapterTable_AddItemText(m_hAdapterTableListBox,EnumDir[i]);
	}
	XEle_RedrawEle(m_hListBox);
	
	
	m_SaveData.SetPage3LangIndex(iItem);
	m_SaveData.Save();
	
	return 0;
}



int CPage3::OnEventKeyDown( WPARAM wParam,LPARAM lParam,BOOL *pbHandled )
{

	if (wParam == VK_RETURN)
	{
		//回车，然后搜索。。。
		wchar_t buffer[MAX_PATH_XC]={0};
		XRichEdit_GetText(m_hRichEdit,buffer,MAX_PATH_XC);
		if (wcslen(buffer) == 0)
		{
			ShowCategory(NULL);
		} 
		else
		{
			ShowCategory(buffer);
		}

	}
	return 0;
}

void CPage3::ShowCategory( const wchar_t* lpCategory )
{

	XAdapterTable_DeleteItemAll(m_hAdapterTableListBox);
	
	//枚举目录名称，然后找到关键字相关的显示

	wchar_t lpszType[MAX_PATH_XC]={0};
	XRichEdit_GetText(m_hComboBox,lpszType,MAX_PATH_XC);

	wchar_t lpFullPath[MAX_PATH_XC]={0};
	wsprintfW(lpFullPath,L"%s\\%s",m_pLuaUIMgrDir,lpszType);

	CEnumPath enumDir(lpFullPath);
	
	if (lpCategory == NULL)
	{
		for (int i = 0; i < enumDir.GetItemCount(); i++)
		{
			XAdapterTable_AddItemText(m_hAdapterTableListBox,enumDir[i]);
		}
		
	} 
	else
	{
		for (int i = 0; i < enumDir.GetItemCount(); i++)
		{
			if (StrStrIW(enumDir[i],lpCategory))
			{
				XAdapterTable_AddItemText(m_hAdapterTableListBox,enumDir[i]);
			}
		}
		
		
	}
	
	XEle_RedrawEle(m_hListBox);
}

int CPage3::OnListBoxTemplateCreateEnd( listBox_item_i* pItem,BOOL *pbHandled )
{
	HXCGUI hBtn = XListBox_GetTemplateObject(m_hListBox,pItem->index,3);
	if (XC_IsHXCGUI(hBtn,XC_BUTTON))
	{
		XEle_RegEventCPP1((HELE)hBtn,XE_BNCLICK,&CPage3::OnBtnClick);
	}
	return 0;
}

int CPage3::OnBtnClick( HELE hEventEle,BOOL *pbHandled )
{
	int nIndex = XListBox_GetItemIndexFromHXCGUI(m_hListBox,hEventEle);

	wchar_t langType[MAX_PATH_XC]={0};
	XRichEdit_GetText(m_hComboBox,langType,MAX_PATH_XC);


	wchar_t dirName[MAX_PATH_XC] = {0};
	XAdapterTable_GetItemText(m_hAdapterTableListBox,nIndex,0,dirName,MAX_PATH_XC);
	
	wchar_t fullDir[MAX_PATH_XC]={0};
	wsprintfW(fullDir,L"%s\\%s\\%s",m_pLuaUIMgrDir,langType,dirName);
	
	XC_OpenFloder(fullDir);
	
	return 0;
}

int CPage3::OnBtnCategoryClick(HELE hEventEle,BOOL *pbHandled )
{

	wchar_t buffer[MAX_PATH_XC]={0};
	XBtn_GetText(hEventEle,buffer,MAX_PATH_XC);

	if (wcscmp(buffer,L"全部")==0)
	{
		ShowCategory(NULL);
	} 
	else
	{
		ShowCategory(buffer);
	}

	return 0;
}




//////////////////////////////////////////////////////
CWindowXML::CWindowXML()
{

}
int CWindowXML::OnBtnClick(BOOL *pbHandled)
{
	wchar_t pPath[MAX_PATH_XC] = {0};
	XRichEdit_GetText(m_hEditPath,pPath,MAX_PATH_XC);
	XC_OpenFloder(pPath);
	return 0;
}

DWORD WINAPI CheckNewVerThread(LPVOID lpParam)
{
	PostMessage((HWND)lpParam,WM_USER+999,HasNewVer(),0);

	return 0;
}

int CWindowXML::OnWndSetTextLinkText(WPARAM wParam,LPARAM lParam,BOOL *pbHandled)
{
	if (wParam)
	{
		XBtn_SetText(hTextLink,L"点击下载新版");
		XBtn_SetTextAlign(hTextLink,DT_VCENTER|DT_CENTER);
		XEle_SetTextColor(hTextLink,255,255);
		XEle_RegEventCPP(hTextLink,XE_BNCLICK,&CWindowXML::OnTextLinkClick);
	}else
	{
		wchar_t helperVer[MAX_PATH_XC] = {0};
		wsprintfW(helperVer,L"助手版本:%d",XCGUIHELPERVER);
		XBtn_SetText(hTextLink,helperVer);
		XBtn_SetTextAlign(hTextLink,DT_VCENTER|DT_CENTER);
		XEle_SetTextColor(hTextLink,RGB(172,172,172),255);
		XEle_Enable(hTextLink,FALSE);
	}
	*pbHandled = TRUE;
	return 0;
}

int CWindowXML::OnWndSize( UINT nFlags,SIZE *pSize,BOOL *pbHandled)
{
	m_SaveData.SetWindowSize(pSize->cx,pSize->cy);
	m_SaveData.Save();
	return 0;
}

int CWindowXML::OnWndMove(WPARAM wParam,LPARAM lParam,BOOL *pbHandled)
{

	int xPos = (int)(short) LOWORD(lParam);   // horizontal position 
	int yPos = (int)(short) HIWORD(lParam);   // vertical position 

	if (xPos < 0 || yPos < 0)
		return 0;

	m_SaveData.SetWindowPos(xPos,yPos);
	m_SaveData.Save();

	return 0;
}

VOID CWindowXML::Create()
{
	wchar_t curDir[MAX_PATH_XC] = {0};
	MyGetDirectory(curDir,MAX_PATH_XC);

	wchar_t pathRes[MAX_PATH_XC]={0};
	wchar_t layoutResDir[MAX_PATH_XC] = {0};
	wsprintfW(pathRes, L"%s\\%s", curDir,  XC_FULLPATH_LAYOUT_RES_FILE);
	wsprintfW(layoutResDir,L"%s\\%s",curDir,XC_PATH_LAYOUTRES_DIR);
	if(FALSE==XC_LoadResource(pathRes,layoutResDir))
		return ;

	wchar_t pathLayout[MAX_PATH_XC]={0};
	wsprintfW(pathLayout, L"%s\\%s", curDir, XC_FULLPATH_LAYOUT_FILE);
	m_hWindow = (HWINDOW) XC_LoadLayout(pathLayout);
	if(NULL==m_hWindow) return ;

	//如果存在配置文件就加载配置，否则不加载
	if (m_SaveData.IsExsitSetFile())
	{
		int x,y,cx,cy;
		m_SaveData.GetWindowPos(x,y);
// 		RECT rtWindow;
// 		GetWindowRect(XWnd_GetHWND(m_hWindow),&rtWindow);
		m_SaveData.GetWindowSize(cx,cy);
		MoveWindow(XWnd_GetHWND(m_hWindow),x,y,cx,cy,FALSE);
	}


	XWnd_RegEventCPP(m_hWindow,WM_SIZE,&CWindowXML::OnWndSize);
	XWnd_RegEventCPP(m_hWindow,WM_MOVE,&CWindowXML::OnWndMove);



	m_Page1.Create(m_hWindow);
	m_Page2.Create(m_hWindow);
	m_Page3.Create(m_hWindow);


	HELE hBtn = (HELE)XC_GetObjectByID(m_hWindow,20);
	XEle_RegEventCPP(hBtn,XE_BNCLICK,&CWindowXML::OnBtnClick);

	m_hEditPath = (HELE) XC_GetObjectByID(m_hWindow,501);


	hBtn1 = XC_GetHEleByID(m_hWindow,ID_Main_button1);
	hBtn2 = XC_GetHEleByID(m_hWindow,ID_Main_button2);
	hBtn3 = XC_GetHEleByID(m_hWindow,ID_Main_button3);


	wchar_t szImageButton[MAX_PATH_XC] = {0};
	HIMAGE hImage = NULL;

	for (int i = 1; i < 9; i++)
	{
		wsprintfW(szImageButton,L"%s\\%s\\Hover_0%d.png",curDir,XC_FULLPATH_IMAGE_MAINWND_PATH,i);
		hImage = XImage_LoadFile(szImageButton);
		XBtn_AddAnimationFrame(hBtn1,hImage,40);
		XBtn_AddAnimationFrame(hBtn2,hImage,40);
		XBtn_AddAnimationFrame(hBtn3,hImage,40);

	}
	
	XBtn_EnableAnimation(hBtn1,TRUE);
	XBtn_EnableAnimation(hBtn2,TRUE);
	XBtn_EnableAnimation(hBtn3,TRUE);


//	hBtn4 = (HELE) XC_GetObjectByID(m_hWindow,ID_Main_button4);

// 	hPage1 = (HELE) XC_GetObjectByID(m_hWindow,200);
// 	hPage2 = (HELE) XC_GetObjectByID(m_hWindow,300);
// 	hPage3 = (HELE) XC_GetObjectByID(m_hWindow,400);

	XEle_SetUserData(hBtn1,0);
	XEle_SetUserData(hBtn2,1);
	XEle_SetUserData(hBtn3,2);
//	XEle_SetUserData(hBtn4,3);

	XEle_RegEventCPP1(hBtn1,XE_BNCLICK,&CWindowXML::OnHeadBtnClick);
	XEle_RegEventCPP1(hBtn2,XE_BNCLICK,&CWindowXML::OnHeadBtnClick);
	XEle_RegEventCPP1(hBtn3,XE_BNCLICK,&CWindowXML::OnHeadBtnClick);
//	XEle_RegEventCPP1(hBtn4,XE_BNCLICK,&CWindowXML::OnHeadBtnClick);

	HELE hCurSelectBtn = NULL; 
	switch (m_SaveData.GetMainTabSelect())
	{
	case 0:
		hCurSelectBtn = hBtn1;
		break;
	case 1:
		hCurSelectBtn = hBtn2;
		break;
	case 2:
		hCurSelectBtn = hBtn3;
		break;
	case 3:
		hCurSelectBtn = hBtn4;
		break;
	}
	
	XEle_PostEvent(hCurSelectBtn,hCurSelectBtn,XE_BNCLICK,NULL,NULL);
	XBtn_SetCheck(hCurSelectBtn,TRUE);

	

	hTextLink = (HELE)XC_GetObjectByID(m_hWindow,25);

	XWnd_RegEventCPP(m_hWindow,WM_USER+999,&CWindowXML::OnWndSetTextLinkText);

	PostMessage(XWnd_GetHWND(m_hWindow),WM_USER+999,0,0);

	
//	CreateThread(NULL,NULL,CheckNewVerThread,XWnd_GetHWND(m_hWindow),0,NULL);


	HXCGUI hShapeTextXCGUIVer = XC_GetObjectByID(m_hWindow,2000);
	XShapeText_SetText(hShapeTextXCGUIVer,XcguiVer);

	XWnd_AdjustLayout(m_hWindow);
	XWnd_ShowWindow(m_hWindow,SW_SHOW);
}

int CWindowXML::OnTextLinkClick(BOOL* pbHandled)
{

// 	if (HasNewVer())
// 	{
// 		if (MessageBoxA(NULL,"编程助手发现新版本，是否打开下载？","提示",MB_OKCANCEL) == IDOK)
// 		{
		XC_OpenFloder(L"http://www.xcgui.com/bbs/forum.php?mod=viewthread&tid=1638&page=1&extra=#pid12683");
// 		}
// 	}
	*pbHandled = TRUE;
	return 0;
}

int CWindowXML::OnHeadBtnClick(HELE hEventEle,BOOL *pbHandled)
{
	m_SaveData.SetMainTabSelect(XEle_GetUserData(hEventEle));
	m_SaveData.Save();
	return 0;
}



////////////////////////////////////////////////////////////

CCmdLine::CCmdLine()
{
	m_lpCmd = CommandLineToArgvW(GetCommandLineW(),&m_nArc);
}
wchar_t* CCmdLine::operator[](int nIndex)
{
	return m_lpCmd[nIndex];
}
CCmdLine::~CCmdLine()
{
	GlobalFree(m_lpCmd);
}



#if 1
int APIENTRY WinMain(HINSTANCE hInstance,
					 HINSTANCE hPrevInstance,
					 LPSTR     lpCmdLine,
					 int       nCmdShow)
{
	//初始化软件所在目录
	InitAppCurrentDir();

	XInitXCGUI();
	XC_EnableDebugFile(FALSE);
	CWindowXML XML;
	XML.Create();
	XRunXCGUI();
	XExitXCGUI();


	return 0;
}
#endif