#include "Common.h"

class CWindow_Demo
{
public:
    HWINDOW m_hWindow;
    CWindow_Demo()
    {
        Init();
    }
    void Init()
    {
        m_hWindow = XWnd_Create(0, 0, 300, 200, L"�Ųʽ���ⴰ��",NULL, xc_window_style_default);
        XBtn_SetType(XBtn_Create(5, 3, 60, 20, L"Close", m_hWindow),button_type_close);

		XBtn_Create(20,70,100,20,L"����",m_hWindow);

        HELE hComboBox=XComboBox_Create(20,40,120,20,m_hWindow);
        XComboBox_SetItemTemplateXML(hComboBox,L"ComboBox_ListBox_Item.xml");
        XRichEdit_SetText(hComboBox,L"123");

        HXCGUI hAdapter=XAdapterTable_Create();
        XComboBox_BindApapter(hComboBox,hAdapter);
        XAdapterTable_AddColumn(hAdapter,L"name");

        wchar_t  buf[256]={0};
        for (int i=0;i<20;i++)
        {
            wsprintfW(buf,L"name-%d-0",i);
            XAdapterTable_AddItemText(hAdapter,buf);
        }

		XEle_RegEventCPP(hComboBox,XE_COMBOBOX_POPUP_LIST,&CWindow_Demo::OnComboBoxPopupList);

		XWnd_AdjustLayout(m_hWindow);
        XWnd_ShowWindow(m_hWindow,SW_SHOW);
    }
	int OnComboBoxPopupList(HWINDOW hWindow,HELE hListBox,BOOL *pbHandled)
	{
		XEle_EnableBkTransparent(hListBox,TRUE);

		HBKINFOM hBkInfoM = XEle_GetBkInfoManager(hListBox);
		XBkInfoM_AddFill(hBkInfoM,element_state_flag_enable,0,255);
		XBkInfoM_AddFill(hBkInfoM,listBox_state_flag_item_leave,RGB(0,128,0),255);
		XBkInfoM_AddFill(hBkInfoM,listBox_state_flag_item_stay,RGB(0,0,128),255);

		XEle_SetAlpha(hListBox,100);
// 		HBKINFOM hBkInfoM = XWnd_GetBkInfoManager(hWindow);
// 		XBkInfoM_AddFill(hBkInfoM,window_position_window,RGB(128,128,0),255);
 		XWnd_EnableDrawBk(hWindow,FALSE);

		XWnd_SetTransparentType(hWindow,window_transparent_shadow);
		XWnd_SetTransparentAlpha(hWindow,10);


		XSView_SetScrollBarSize(hListBox,10);

//		XWnd_SetLayoutSize(hWindow,10,10,10,10);
//		HWINDOW hw = XEle_GetHWINDOW(hListBox);
		return 0;
	}
};

int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
    XInitXCGUI();
    CWindow_Demo  MyWindow;
    XRunXCGUI();
    XExitXCGUI();
    return TRUE;
}
